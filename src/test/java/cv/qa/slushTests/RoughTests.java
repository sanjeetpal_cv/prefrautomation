package cv.qa.slushTests;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import cv.qa.drivers.WebDriverManager;
import cv.qa.enums.TenantConfig;
import cv.qa.utils.JsonUtils;
import cv.qa.utils.ScreenshotUtil;
import org.testng.annotations.Test;

public class RoughTests {

    @Test
    public void propertyJson() throws Exception {
        JsonUtils.get(TenantConfig.TENANTINDIALENDS);
        System.out.println(JsonUtils.get(TenantConfig.INDIALENDSAPIKEY));
    }

    @Test
    public void screenshotTest(){
        WebDriverManager.getDriver().get("https://test-cvloan.creditvidya.com/indialends");

        ExtentReports extent = new ExtentReports();
        ExtentTest test = extent.createTest("First Test");
        test.pass("Screenshot at ", MediaEntityBuilder.createScreenCaptureFromBase64String(ScreenshotUtil.getBase64Image()).build());

    }
}
