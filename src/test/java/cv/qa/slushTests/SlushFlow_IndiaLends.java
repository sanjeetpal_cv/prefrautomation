package cv.qa.slushTests;

import cv.qa.base.UiBase;
import cv.qa.dataProviders.SlushDataProvider;
import cv.qa.drivers.Driver;
import cv.qa.drivers.WebDriverManager;
import cv.qa.slushScreens.*;
import cv.qa.thridPartyScreens.DigioScreen;
import cv.qa.utils.zeroCell.SlushMasterData;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public final class SlushFlow_IndiaLends extends UiBase {

    private SlushFlow_IndiaLends(){

    }

    @BeforeClass
    public void launchBrowser() {
        Driver.initWebDriver("chrome");
        WebDriverManager.getDriver().get("https://test-cvloan.creditvidya.com/indialends");
    }


    @Test( priority = 1)
    public void completeSlushScreen() {
        new SlushLaunchScreen()
                .clickActivateNow();
    }

    @Test(priority = 2, dataProvider= "pl_app_data_slush_master", dataProviderClass = SlushDataProvider.class)
    public void mobileVerification(SlushMasterData data) {
        new SlushMobileVerificationScreen()
                .enterMobileNumber(data.getMobileNumber())
                .submitMobileVerification();
    }


    @Test(priority = 3, dataProvider= "pl_app_data_slush_master", dataProviderClass = SlushDataProvider.class)
    public void submitOtp(SlushMasterData data) {
                new SlushOtpVerificationScreen()
                .enterOtp(data.getLoginOtp())
                .clickOnsubmitOtp();
    }

    @Test(priority = 4, dataProvider= "pl_app_data_slush_master", dataProviderClass = SlushDataProvider.class)
    public void fillEmploymentDetails(SlushMasterData data){
        new SlushEmploymentDetailsScreen()
                .selectEmpType(data.getEmploymentType())
                .selectOrgType(data.getOrganizationType())
                .enterNetMonthlyIncome(data.getNetMonthlyIncome())
                .submitEmploymentDetails();
    }

    @Test(priority = 5, dataProvider= "pl_app_data_slush_master", dataProviderClass = SlushDataProvider.class)
    public void providePersonalDetails(SlushMasterData data){
        new SlushPersonalDetailsScreen()
                .enterFirstName(data.getFirstName())
                .enterLastName(data.getLastName())
                .enterMothersName(data.getMotherName())
                .selectGender(data.getGender())
                .selectDob(data.getDob_year(), data.getDob_month(), data.getDob_date())
                .enterPersonalEmail(data.getPersonalEmail())
                .enterPanNumber(data.getPanNumber())
                .enterRequiredLoanAmount(data.getRequiredLoanAmount())
                .selectPurposeOfLoan(data.getLoanPurpose())
                .submitPersonalDetails();
    }

    @Test(priority = 6, dataProvider= "pl_app_data_slush_master", dataProviderClass = SlushDataProvider.class)
    public void provideAddressDetails(SlushMasterData data){
        new SlushAddressDetailsScreen()
                .selectCurrentAddressType(data.getCurrentResidenceType())
                .enterCurrentAddress(data.getCurrentAddress())
                .enterCurrentAddressPincode(data.getCurrentAddressPincode())
                .selectIsAddressTypePermanent(data.getIsCurrAddIsPermanentAdd())
                .submitAddressDetails();
    }

    @Test(priority = 7, dataProvider= "pl_app_data_slush_master", dataProviderClass = SlushDataProvider.class)
    public void provideWorkDetails(SlushMasterData data){
        new SlushWorkDetailsScreen()
//                .enterBusinessName(data.getBusinessName())
//                .enterBusinessAddress(data.getBusinessAddress())
//                .enterBusinessAddressPincode(data.getBusinessPincode())
//                .selectBusinessPlaceOwnership(data.getBusinessPlaceOwnership())
//                .selectHowOldIsBusiness(data.getHowOldIsBusiness())
//                .selectItrFiled(data.getItrFiled())
//                .selectGstFiled(data.getGstFiled())
//                .provideBureauConsent()
//                .submitWorkDetails();

                .enterCurrentCompanyName(data.getCurrCompanyName())
                .enterWorkEmail(data.getWorkEmail())
                .enterOfficeAddressPincode(data.getOfficePincode())
                .selectWorkingSinceInCurrentOrganization(data.getWorkingSince_years(), data.getWorkingSince_months())
                .provideBureauConsent()
                .submitWorkDetails();
    }

    @Test(priority = 8, dataProvider= "pl_app_data_slush_master", dataProviderClass = SlushDataProvider.class)
    public void answerCibilQuestions(SlushMasterData data){
        new SlushCreditBureau()
                .selectAnswerOptions_stub(1,1);

    }
    @Test(priority = 9, dataProvider= "pl_app_data_slush_master", dataProviderClass = SlushDataProvider.class)
    public void selectOffer(SlushMasterData data){
        new SlushOfferScreen()
                .selectOfferAndProceed();
//        new SlushTentativeOfferScreen()
//                .uploadBankStatement("hdfc bank","BankStatement1.pdf","")
//                .saveStatementAndProceed();
    }

    @Test(priority = 10, dataProvider= "pl_app_data_slush_master", dataProviderClass = SlushDataProvider.class)
    public void uploadDocuments(SlushMasterData data){
        new SlushUploadDocumentsScreen()
                .proceedUploadingDocuments()
                .uploadSelfie("Selfie_CaptianAmerica.png")
                .proceedUploadingDocuments();

    }

    @Test(priority = 11, dataProvider= "pl_app_data_slush_master", dataProviderClass = SlushDataProvider.class)
    public void completeKyc(SlushMasterData data){
         new SlushAllKycScreen()
                .selectKycList_DrivingLicence()
                .enterDrivingLicenseNumber(data.getDrivingLicenseNumber())
                .selectDlDob(data.getDl_dob_year(), data.getDl_dob_month(), data.getDl_dob_date())
                .submitDrivingLicenceDetails()
                .uploadDlImagesFrontAndBack("selfie.jpg","selfie.jpg")
                .uploadDl_submit();

        new SlushUploadDocumentsScreen()
                .confirmCurrentAddressSameAsKyc(data.getCurrAdd_sameAs_aadhar())
                .proceedFromCurrentAddress()
                .selectCurrentAddressProofType("Water bill")
                .uploadCurrentAddressProof("selfie.jpg")
                .proceedUploadingDocuments();

    }

    @Test(priority = 12, dataProvider= "pl_app_data_slush_master", dataProviderClass = SlushDataProvider.class)
    public void setupRepayment(SlushMasterData data){
        new SlushRepaymentSetupIntroScreen()
                .proceedRepaymentSetup();
        new SlushRepaymentAccountDetailsScreen()
                .nachSetupNonPrimaryBank();
        new SlushUploadDocumentsScreen()
                .uploadNachProof("BankStatement1.pdf");
        new SlushRepaymentAccountDetailsScreen()
                .selectBank(data.getNach_setup_primary_bank())
                .enterAccountNumber(data.getNach_accountNumber())
                .enterIfsc(data.getNach_ifsc())
                .selectRepaymentMode("net banking")
                .submitRepaymentSetup();
        new DigioScreen()
                .submitDigioConsent();
    }


}

