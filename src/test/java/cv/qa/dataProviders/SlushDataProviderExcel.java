package cv.qa.dataProviders;

import cv.qa.constants.FrameworkConstants;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class SlushDataProviderExcel {

    @DataProvider(name = "slushApplication_Data_excel")
    public Object[][] getExcelData() throws IOException {
        Object[][] obj = new Object[0][];
        try {

            String filepath = FrameworkConstants.getSlushApplicationDataFilePath();
            File file = new File(filepath);
            FileInputStream fileInputStream = new FileInputStream(file);
            XSSFWorkbook wb = new XSSFWorkbook(fileInputStream);
            XSSFSheet sheet = wb.getSheet("slushMasterData");
//            XSSFSheet sheet = wb.getSheetAt(2);

            int rowCount = sheet.getLastRowNum();
            int colCount = sheet.getRow(0).getLastCellNum();


            obj = new Object[rowCount][1];

            for (int i = 0; i < rowCount; i++) {
                HashMap<Object, Object> dataMap = new HashMap<Object, Object>();
                for (int j = 0; j < colCount; j++) {
                    Cell key = sheet.getRow(0).getCell(j);
                    Cell value = sheet.getRow(i + 1).getCell(j);

                    DataFormatter df = new DataFormatter();
                    Object newValue = df.formatCellValue(value);

                    dataMap.put(key,newValue);
                }
                obj[i][0] = dataMap;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return obj;
    }


    @DataProvider(name = "slushApplication_Data_excel")
    public Object[] getExcelData1() throws IOException {
             String filepath = FrameworkConstants.getSlushApplicationDataFilePath();
            File file = new File(filepath);
            FileInputStream fileInputStream = new FileInputStream(file);
            XSSFWorkbook wb = new XSSFWorkbook(fileInputStream);
            XSSFSheet sheet = wb.getSheet("slushMasterData");
//            XSSFSheet sheet = wb.getSheetAt(2);

            int rowNum = sheet.getLastRowNum();
            int colNum = sheet.getRow(0).getLastCellNum();

            Object[] data = new Object[rowNum];
            Map<String,String> map;

            for (int i = 1; i < rowNum; i++) {
                map = new HashMap<>();
                for (int j = 0; j < colNum; j++) {


                    String key = sheet.getRow(0).getCell(j).getStringCellValue();
                    String value = sheet.getRow(i).getCell(j).getStringCellValue();
                    map.put(key,value);
                    data[i-1]=map;
                }
            }
            return data;
    }

    @Test
    public void sample(Map<String,String> mapss){
        System.out.println(mapss.isEmpty());
        System.out.println(mapss.toString());
        System.out.println(mapss.get("annualTurnover"));

        System.out.println("printing  --> "+ mapss.get("personalEmail"));
    }

    @Test
    public void mapTest(){
        HashMap<String,String> testMap = new HashMap();
        testMap.put("loanPurpose","Vacation");
        testMap.put("officePincode","");

        System.out.println(testMap);
        System.out.println(testMap.get("loanPurpose"));
    }
}
