package cv.qa.dataProviders;

import cv.qa.constants.FrameworkConstants;
import cv.qa.utils.ExcelUtils1;
import cv.qa.utils.zeroCell.SlushMasterData;
import io.github.sskorol.core.DataSupplier;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.DataProvider;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SlushDataProvider {
    @DataSupplier(name = "pl_app_data_slush_master")
    public static Object[] getData(){
        List<SlushMasterData> slushUserDataList = SlushMasterData.readSlushMasterData();
        return slushUserDataList.toArray();
    }







}
