package cv.qa.base;


import cv.qa.constants.FrameworkConstants;
import cv.qa.drivers.WebDriverManager;
import cv.qa.enums.WaitStrategy;
import cv.qa.factories.ExplicitWaitFactory;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;

import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;

public class UiBase {

    protected void click(By by, WaitStrategy waitStrategy) {
       WebElement element = ExplicitWaitFactory.performExplicitWait(waitStrategy,by);
       element.click();
    }

    protected void clickWithString(By by,String elementText, WaitStrategy waitStrategy) {
        WebElement element = ExplicitWaitFactory.performExplicitWait(waitStrategy,by);
        element.click();
    }

    protected void sendKeys (By by, String value, WaitStrategy waitStrategy){
        WebElement element = ExplicitWaitFactory.performExplicitWait(waitStrategy,by);
        element.sendKeys(value);
    }

    protected void uploadFileSendKeys (By by, String fileNameWithExtension, WaitStrategy waitStrategy){
        WebElement element = ExplicitWaitFactory.performExplicitWait(waitStrategy,by);
        String filePath = FrameworkConstants.getSlushJourneyFiles()+fileNameWithExtension;
        element.sendKeys(filePath);
    }

    protected void uploadFileRobotClass (By by, String fileNameWithExtension, WaitStrategy waitStrategy){
        WebElement element = ExplicitWaitFactory.performExplicitWait(waitStrategy,by);
        JavascriptExecutor js = (JavascriptExecutor) WebDriverManager.getDriver();
        js.executeScript("arguments[0].click();",element);
        String filePath = FrameworkConstants.getSlushJourneyFiles()+fileNameWithExtension;
        try {
            Robot robot = new Robot();
            robot.delay(1000);
            StringSelection filePathRobot = new StringSelection(filePath);
            Toolkit.getDefaultToolkit().getSystemClipboard().setContents(filePathRobot,null);

            robot.keyPress(KeyEvent.VK_CONTROL);
            robot.keyPress(KeyEvent.VK_V);

            robot.keyRelease(KeyEvent.VK_CONTROL);
            robot.keyRelease(KeyEvent.VK_V);

            robot.keyPress(KeyEvent.VK_ENTER);
        } catch (AWTException e) {
            throw new RuntimeException(e);
        }
        element.sendKeys(filePath);
    }

    protected void sendKeysInDropDown (By by, String value, WaitStrategy waitStrategy){
        Actions actions = new Actions(WebDriverManager.getDriver());
        WebElement element = ExplicitWaitFactory.performExplicitWait(waitStrategy,by);
        element.sendKeys(value);
        actions.keyDown(Keys.ENTER);
    }

    protected void getPageTitle (By by, WaitStrategy waitStrategy){
        WebElement element = ExplicitWaitFactory.performExplicitWait(waitStrategy,by);
        element.getText();
    }

    protected String getText(By by, WaitStrategy waitStrategy){
        WebElement element = ExplicitWaitFactory.performExplicitWait(waitStrategy,by);
        String text = element.getText();
        return text;
    }

    protected WebDriver closeBrowser(){
       WebDriverManager.getDriver().quit();
       return WebDriverManager.getDriver();
    }

    protected void selectFromDropdown(By by, WaitStrategy waitStrategy){
        Actions action=new Actions(WebDriverManager.getDriver());
        WebElement element = ExplicitWaitFactory.performExplicitWait(waitStrategy,by);
        action.moveToElement(element).click();
    }

    protected void scrollAndSelectFromDropdown(By by, WaitStrategy waitStrategy){
        JavascriptExecutor je = (JavascriptExecutor) WebDriverManager.getDriver();
        WebElement element = ExplicitWaitFactory.performExplicitWait(waitStrategy,by);
        je.executeScript("arguments[0].scrollIntoView(true)",element);
        element.click();

    }


}
