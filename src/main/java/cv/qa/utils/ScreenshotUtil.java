package cv.qa.utils;

import cv.qa.constants.FrameworkConstants;
import cv.qa.drivers.WebDriverManager;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import java.io.File;
import java.io.IOException;
import java.util.Date;

public class ScreenshotUtil {

    public static String screenshotPath;
    public static String screenshotName;

    public static void captureScreenshot_JPG() throws IOException {

        File scrFile = ((TakesScreenshot) WebDriverManager.getDriver()).getScreenshotAs(OutputType.FILE);

        Date d = new Date();
        screenshotName = d.toString().replace(":", "_").replace(" ", "_") + ".jpg";

        FileUtils.copyFile(scrFile,
                new File(FrameworkConstants.getConfigFilePath()+ System.getProperty("user.dir") + "\\target\\reports\\html\\" + screenshotName));

    }

    public static String getBase64Image() {
        return ((TakesScreenshot)WebDriverManager.getDriver()).getScreenshotAs(OutputType.BASE64);
    }

    public static void main(String[] args) throws IOException {
        captureScreenshot_JPG();
        System.out.println(getBase64Image());
    }



}
