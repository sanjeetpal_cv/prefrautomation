package cv.qa.utils;

import cv.qa.constants.FrameworkConstants;
import cv.qa.enums.ConfigProperties;
import cv.qa.exceptions.PropertyFileUsageException;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;

public final class PropertiesUtil {

    private PropertiesUtil(){

    }

    private static Properties property = new Properties();
    private static final Map<String,String> CONFIG_MAP = new HashMap<>();
//    static FileInputStream file;
//    public static FileInputStream readPropertiesFile(String filePath){
//
//        try{
//            file = new FileInputStream(filePath);
//            property.load(file);
//
//            for (Map.Entry<Object, Object>  entry: property.entrySet()){
//                CONFIG_MAP.put(String.valueOf(entry.getKey()), String.valueOf(entry.getValue()).trim());
//            }
//
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        return file;
//    }



    static {
        try{
            FileInputStream file = new FileInputStream(FrameworkConstants.getEnvironmentConfigPath());
            property.load(file);

            for (Map.Entry<Object, Object>  entry: property.entrySet()){
                CONFIG_MAP.put(String.valueOf(entry.getKey()), String.valueOf(entry.getValue()).trim());
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static String get(ConfigProperties key)  {
        if (Objects.isNull(key) || Objects.isNull(CONFIG_MAP.get(key.name().toLowerCase()))) {
            throw new PropertyFileUsageException("Property name " + key + " is not found. Please check config.properties");
        }
        return CONFIG_MAP.get(key.name().toLowerCase());
    }

}
