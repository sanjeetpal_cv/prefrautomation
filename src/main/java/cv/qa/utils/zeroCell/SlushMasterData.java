package cv.qa.utils.zeroCell;

import com.creditdatamw.zerocell.Reader;
import com.creditdatamw.zerocell.annotation.Column;
import com.creditdatamw.zerocell.annotation.RowNumber;
import cv.qa.constants.FrameworkConstants;

import java.io.File;
import java.util.List;

public class SlushMasterData {

   @Column(index=0, name="scenario")
    private String scenario;

    @Column(index=1, name="dpUserId")
    private String dpUserId;

    @Column(index=2, name="mobileNumber")
    private String mobileNumber;

    @Column(index=3, name="loginOtp")
    private String loginOtp;

    @Column(index=4, name="employmentType")
    private String employmentType;

    @Column(index=5, name="organizationType")
    private String organizationType;

    @Column(index=6, name="businessType")
    private String businessType;

    @Column(index=7, name="workDetails")
    private String workDetails;

    @Column(index=8, name="netMonthlyIncome")
    private String netMonthlyIncome;

    @Column(index=9, name="annualTurnover")
    private String annualTurnover;

    @Column(index=10, name="otherSourceOfIncome")
    private String otherSourceOfIncome;

    @Column(index=11, name="firstName")
    private String firstName;

    @Column(index=12, name="lastName")
    private String lastName;

    @Column(index=13, name="motherName")
    private String motherName;

    @Column(index=14, name="gender")
    private String gender;

    @Column(index=15, name="dob_year")
    private String dob_year;

    @Column(index=16, name="dob_month")
    private String dob_month;

    @Column(index=17, name="dob_date")
    private String dob_date;

    @Column(index=18, name="personalEmail")
    private String personalEmail;

    @Column(index=19, name="panNumber")
    private String panNumber;

    @Column(index=20, name="loanPurpose")
    private String loanPurpose;

    @Column(index=21, name="currentResidenceType")
    private String currentResidenceType;

    @Column(index=22, name="currentAddress")
    private String currentAddress;

    @Column(index=23, name="currentAddressPincode")
    private String currentAddressPincode;

    @Column(index=24, name="isCurrAddIsPermanentAdd")
    private String isCurrAddIsPermanentAdd;

    @Column(index=25, name="currCompanyName")
    private String currCompanyName;

    @Column(index=26, name="workEmail")
    private String workEmail;

    @Column(index=27, name="officePincode")
    private String officePincode;

    @Column(index=28, name="workingSince_years")
    private String workingSince_years;

    @Column(index=29, name="workingSince_months")
    private String workingSince_months;

    @Column(index=30, name="businessName")
    private String businessName;

    @Column(index=31, name="businessAddress")
    private String businessAddress;

    @Column(index=32, name="businessPincode")
    private String businessPincode;

    @Column(index=33, name="businessPlaceOwnership")
    private String businessPlaceOwnership;

    @Column(index=34, name="howOldIsBusiness")
    private String howOldIsBusiness;

    @Column(index=35, name="itrFiled")
    private String itrFiled;

    @Column(index=36, name="gstFiled")
    private String gstFiled;

    @Column(index=37, name="bureauConsent")
    private String bureauConsent;

    @Column(index=38, name="aadhar_mobileLinked_flag")
    private String aadhar_mobileLinked_flag;

    @Column(index=39, name="kyc_proofType")
    private String kyc_proofType;

    @Column(index=40, name="drivingLicenseNumber")
    private String drivingLicenseNumber;

    @Column(index=41, name="dl_dob_year")
    private String dl_dob_year;

    @Column(index=42, name="dl_dob_month")
    private String dl_dob_month;

    @Column(index=43, name="dl_dob_date")
    private String dl_dob_date;

    @Column(index=44, name="currAdd_sameAs_aadhar")
    private String currAdd_sameAs_aadhar;

    @Column(index=45, name="currAddProof_type")
    private String currAddProof_type;

    @Column(index=46, name="currAddProof_protected")
    private String currAddProof_protected;

    @Column(index=47, name="currAddProof_password")
    private String currAddProof_password;

    @Column(index=48, name="nach_setup_primary_bank")
    private String nach_setup_primary_bank;

    @Column(index=49, name="nach_proof_password")
    private String nach_proof_password;

    @Column(index=50, name="nach_bankName")
    private String nach_bankName;

    @Column(index=51, name="nach_accountNumber")
    private String nach_accountNumber;

    @Column(index=52, name="nach_ifsc")
    private String nach_ifsc;

    @Column(index=53, name="search_ifsc_bankName")
    private String search_ifsc_bankName;

    @Column(index=54, name="search_ifsc_bankState")
    private String search_ifsc_bankState;

    @Column(index=55, name="search_ifsc_bankCity")
    private String search_ifsc_bankCity;

    @Column(index=56, name="search_ifsc_bankBranch")
    private String search_ifsc_bankBranch;

    public String getRequiredLoanAmount() {
        return requiredLoanAmount;
    }

    @Column(index=57, name="requiredLoanAmount")
    private String requiredLoanAmount;

    public static List<SlushMasterData> readSlushMasterData(){
        List<SlushMasterData> slushMasterDataList = Reader.of(SlushMasterData.class)
                .from(new File(FrameworkConstants.getSlushApplicationDataFilePath().toString()))
                .sheet("slushMasterData")
                .list();

        String[] data = Reader.columnsOf(SlushMasterData.class);
        return slushMasterDataList;
    }


    public String getScenario() {
        return scenario;
    }

    public String getDpUserId() {
        return dpUserId;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public String getLoginOtp() {
        return loginOtp;
    }

    public String getEmploymentType() {
        return employmentType;
    }

    public String getOrganizationType() {
        return organizationType;
    }

    public String getBusinessType() {
        return businessType;
    }

    public String getWorkDetails() {
        return workDetails;
    }

    public String getNetMonthlyIncome() {
        return netMonthlyIncome;
    }

    public String getAnnualTurnover() {
        return annualTurnover;
    }

    public String getOtherSourceOfIncome() {
        return otherSourceOfIncome;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getMotherName() {
        return motherName;
    }

    public String getGender() {
        return gender;
    }

    public String getDob_year() {
        return dob_year;
    }

    public String getDob_month() {
        return dob_month;
    }

    public String getDob_date() {
        return dob_date;
    }

    public String getPersonalEmail() {
        return personalEmail;
    }

    public String getPanNumber() {
        return panNumber;
    }

    public String getLoanPurpose() {
        return loanPurpose;
    }

    public String getCurrentResidenceType() {
        return currentResidenceType;
    }

    public String getCurrentAddress() {
        return currentAddress;
    }

    public String getCurrentAddressPincode() {
        return currentAddressPincode;
    }

    public String getIsCurrAddIsPermanentAdd() {
        return isCurrAddIsPermanentAdd;
    }

    public String getCurrCompanyName() {
        return currCompanyName;
    }

    public String getWorkEmail() {
        return workEmail;
    }

    public String getOfficePincode() {
        return officePincode;
    }

    public String getWorkingSince_years() {
        return workingSince_years;
    }

    public String getWorkingSince_months() {
        return workingSince_months;
    }

    public String getBusinessName() {
        return businessName;
    }

    public String getBusinessAddress() {
        return businessAddress;
    }

    public String getBusinessPincode() {
        return businessPincode;
    }

    public String getBusinessPlaceOwnership() {
        return businessPlaceOwnership;
    }

    public String getHowOldIsBusiness() {
        return howOldIsBusiness;
    }

    public String getItrFiled() {
        return itrFiled;
    }

    public String getGstFiled() {
        return gstFiled;
    }

    public String getBureauConsent() {
        return bureauConsent;
    }

    public String getAadhar_mobileLinked_flag() {
        return aadhar_mobileLinked_flag;
    }

    public String getKyc_proofType() {
        return kyc_proofType;
    }

    public String getDrivingLicenseNumber() {
        return drivingLicenseNumber;
    }

    public String getDl_dob_year() {
        return dl_dob_year;
    }

    public String getDl_dob_month() {
        return dl_dob_month;
    }

    public String getDl_dob_date() {
        return dl_dob_date;
    }

    public String getCurrAdd_sameAs_aadhar() {
        return currAdd_sameAs_aadhar;
    }

    public String getCurrAddProof_type() {
        return currAddProof_type;
    }

    public String getCurrAddProof_protected() {
        return currAddProof_protected;
    }

    public String getCurrAddProof_password() {
        return currAddProof_password;
    }

    public String getNach_setup_primary_bank() {
        return nach_setup_primary_bank;
    }

    public String getNach_proof_password() {
        return nach_proof_password;
    }

    public String getNach_bankName() {
        return nach_bankName;
    }

    public String getNach_accountNumber() {
        return nach_accountNumber;
    }

    public String getNach_ifsc() {
        return nach_ifsc;
    }

    public String getSearch_ifsc_bankName() {
        return search_ifsc_bankName;
    }

    public String getSearch_ifsc_bankState() {
        return search_ifsc_bankState;
    }

    public String getSearch_ifsc_bankCity() {
        return search_ifsc_bankCity;
    }

    public String getSearch_ifsc_bankBranch() {
        return search_ifsc_bankBranch;
    }
}
