package cv.qa.slushScreens;

import cv.qa.base.UiBase;
import cv.qa.enums.WaitStrategy;
import org.openqa.selenium.By;

public class SlushRepaymentSetupIntroScreen extends UiBase {

    private final By repaymentSetupIntro_header=By.xpath("//*[@id='noCustomHeader']/div[1]/div[2]/div[contains(text(),'Setup auto repayment')]");

    private final By repaymentSetupIntro_proceedButton=By.xpath("//*[@id='accountDetailsSubmit']");

    public SlushRepaymentSetupIntroScreen proceedRepaymentSetup(){
        click(repaymentSetupIntro_proceedButton, WaitStrategy.CLICKABLE);
        return this;
    }


}
