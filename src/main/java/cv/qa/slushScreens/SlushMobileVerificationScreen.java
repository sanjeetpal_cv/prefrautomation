package cv.qa.slushScreens;

import cv.qa.base.UiBase;
import cv.qa.enums.WaitStrategy;
import org.openqa.selenium.By;

public final class SlushMobileVerificationScreen extends UiBase {

    private String screenName_mobileVerification="//div[@class=\"cvLoanHeaderTitle\"]";

    private String inputMobileNumber_mV="//input[@id=\"mobileNumber\"]";

    private String checkboxBureau_mV="//*[@id=\"checker\"]/label/span";

    private String readMoreBureau_mV="//*[@id=\"checker\"]/div/p[2]";

    private String minimizeBureau_mV="//*[@id=\"checker\"]/div/p[7]";

    private String buttonNext_mV="//div[@class=\"mobile-sim-next\"]";

    private String errorSnackBar_mV="//div[@id=\"snackbar\"]";

    public SlushMobileVerificationScreen getMobileVerificationScreenName(){
        getPageTitle(By.xpath(screenName_mobileVerification), WaitStrategy.VISIBLE);
        return this;
    }

    public SlushMobileVerificationScreen enterMobileNumber(String mobileNumber){
        sendKeys(By.xpath(inputMobileNumber_mV),mobileNumber,WaitStrategy.PRESENCE);
        return this;
    }

    public SlushMobileVerificationScreen selectBureauConsent(){
        click(By.xpath(checkboxBureau_mV),WaitStrategy.PRESENCE);
        return this;
    }

    public SlushMobileVerificationScreen submitMobileVerification(){
        click(By.xpath(buttonNext_mV),WaitStrategy.CLICKABLE);
        return this;
    }

}
