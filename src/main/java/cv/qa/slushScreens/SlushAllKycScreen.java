package cv.qa.slushScreens;

import cv.qa.base.UiBase;
import cv.qa.enums.WaitStrategy;
import org.openqa.selenium.By;

public final class SlushAllKycScreen extends UiBase {

    private final By screenName_Kyc= By.xpath("//*[@class='cvLoanHeaderTitle' and contains(., 'KYC Proof')]");

//KYC DigiLocker
    private final By kycType_Digilocker= By.xpath("//*[@id='noCustomHeader']/div[2]/div/div[1]/div[1]/div[2]");


    public SlushAllKycScreen completeKyc_Digilocker(){
        click(kycType_Digilocker,WaitStrategy.PRESENCE);
        return this;
    }

//KYC OKYC
    private final By kycType_okyc= By.xpath("//*[@id='noCustomHeader']/div[2]/div/div[2]/div[1]/div[2]");

    public SlushAllKycScreen completeKyc_Okyc(){
        click(kycType_okyc,WaitStrategy.PRESENCE);
        return this;
    }
//KYC Driving Licence
    private final By kycType_DrivingLicence= By.xpath("//*[@id='noCustomHeader']/div[2]/div/div[3]/div[1]/div[2]");
    private final By kyc_dl_licenceNumber_input= By.xpath("//*[@id='dlNumber']");

    private final By dobLabel_perDet = By.xpath("//*[@id='dobInput']/label");
    private final By dlDobYear_dropdown=By.xpath("//*[@id='yearBlock']/div/div[2]/span");
    private final By dlDobYear_input=By.xpath("//input[@id='yearDropdown']");
    private final By dlDobYear_dropdownDiv=By.xpath("//*[@id='yearBlock']/div/div[3]");
    private final By dlDobMonth_dropdown=By.xpath("//*[@id='monthBlock']/div/div[2]/span");
    private final By dlDobMonth_input=By.xpath("//input[@id='monthDropdown']");
    private final By dlDobMonth_dropdownDiv=By.xpath("//*[@id='monthBlock']/div/div[3]");
    private final By dlDobDate_dropdown=By.xpath("//*[@id='dateBlock']/div/div[2]/span");
    private final By dlDobDate_input= By.xpath("//input[@id='dateDropdown']");
    private final By dlDobDate_dropdownDiv=By.xpath("//*[@id='dateBlock']/div/div[3]");
    private final By dlDob_SubmitButton=By.xpath("//*[@id='uploadDocumentsSubmitButton']");
    private final By dlImage_frontPage_Upload=By.xpath("//*[@id='files_frontDl']");
    private final By dlImage_backPage_Upload=By.xpath("//*[@id='files_backDl']");

    private final By dlImage_submit=By.xpath("//*[@id='uploadDocumentsSubmitButton'  and contains(text(),'PROCEED')]");


    public SlushAllKycScreen selectKycList_DrivingLicence(){
        click(kycType_DrivingLicence,WaitStrategy.PRESENCE);
        return this;
    }

    public SlushAllKycScreen enterDrivingLicenseNumber(String dlNumber){
        sendKeys(kyc_dl_licenceNumber_input,dlNumber,WaitStrategy.CLICKABLE);
        return this;
    }

    public SlushAllKycScreen selectDlDob(String year, String month, String date){
        click(dlDobYear_dropdown,WaitStrategy.CLICKABLE);
        sendKeys(dlDobYear_input,year,WaitStrategy.CLICKABLE);
        click(dlDobYear_dropdownDiv,WaitStrategy.PRESENCE);

        click(dlDobMonth_dropdown,WaitStrategy.CLICKABLE);
        sendKeys(dlDobMonth_input,month,WaitStrategy.CLICKABLE);
        click(dlDobMonth_dropdownDiv,WaitStrategy.PRESENCE);

        click(dlDobDate_dropdown,WaitStrategy.CLICKABLE);
        sendKeys(dlDobDate_input,date,WaitStrategy.CLICKABLE);
        click(dlDobDate_dropdownDiv,WaitStrategy.PRESENCE);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        return this;
    }

    public SlushAllKycScreen submitDrivingLicenceDetails(){
        click(dlDob_SubmitButton,WaitStrategy.CLICKABLE);
        return this;
    }

    public SlushAllKycScreen uploadDlImagesFrontAndBack(String dlFrontPage, String dlBackPage){
        uploadFileRobotClass(dlImage_frontPage_Upload,dlFrontPage,WaitStrategy.PRESENCE);
        uploadFileRobotClass(dlImage_backPage_Upload,dlBackPage,WaitStrategy.PRESENCE);
        return this;
    }

    public SlushAllKycScreen uploadDl_submit(){
        click(dlImage_submit,WaitStrategy.CLICKABLE);
        return this;
    }





}
