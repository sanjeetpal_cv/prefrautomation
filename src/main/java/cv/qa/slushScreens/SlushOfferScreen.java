package cv.qa.slushScreens;

import cv.qa.base.UiBase;
import cv.qa.enums.WaitStrategy;
import org.openqa.selenium.By;

public class SlushOfferScreen extends UiBase {

    private final By screenName_offScr= By.xpath("//*[@class='cvLoanHeaderTitle']/span");

    private final By approvedAmount_offScr= By.xpath("//*[@class='amountApproved']");

    private final By reqLoanAmt_offScr= By.xpath("//*[@id='amountNeeded']");

    private final By firstTenure_offScr= By.xpath("//*[@id='emi-slider']/div/div/div[1]/div");

    private final By submitSelectedOffer_offScr= By.xpath("//*[@class='btn' and contains(text(),'Confirm')]");

    public  SlushOfferScreen selectOfferAndProceed(){
        click(submitSelectedOffer_offScr,WaitStrategy.CLICKABLE);
        return this;
    }

    private final By offerFeedbackLink_offScr= By.xpath("//*[@class='offerFeebackText' and contains(text(),'Didn’t like the offer')]");

    private final By feedbackModalOverlay_offScr= By.xpath("//*[@id='loanModal']/div[2]");

    private final By feedbackModalClose_offScr= By.xpath("//*[@id='loanModal']//*[@class='loanModalClose']");

    private final By feedbackModalList_offScr= By.xpath("//*[@id='radioBlock']/div");

    private final By feedbackModalSubmit_offScr= By.xpath("//*[@class='loanModalFooter']//*[@type='submit']");

}
