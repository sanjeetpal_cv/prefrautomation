package cv.qa.slushScreens;

import cv.qa.base.UiBase;
import cv.qa.enums.WaitStrategy;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Wait;

public class SlushWorkDetailsScreen extends UiBase {

    private final By screenName_workDet= By.xpath("//*[@class='cvLoanHeaderCol']");

    private final By bizNameLabel_workDet= By.xpath("//*[@id='businessNameInput']/label");

//Self Employed

    private final By bizNameInput_workDet= By.xpath("//*[@id='businessName']");

    public  SlushWorkDetailsScreen enterBusinessName(String businessName){
        sendKeys(bizNameInput_workDet,businessName, WaitStrategy.CLICKABLE);
        return this;
    }

    private final By bizAddLabel_workDet= By.xpath("//*[@id='businessAddressInput']/label");

    private final By bizAddInput_workDet= By.xpath("//*[@id='businessAddress']");

    public  SlushWorkDetailsScreen enterBusinessAddress(String businessAddress){
        sendKeys(bizAddInput_workDet,businessAddress, WaitStrategy.CLICKABLE);
        return this;
    }

    private final By bizPincodeLabel_workDet= By.xpath("//*[@id='businessPincodeInput']/label");

    private final By bizPincodeInput_workDet= By.xpath("//*[@id='businessPincode']");

    public  SlushWorkDetailsScreen enterBusinessAddressPincode(String businessAddressPincode){
        sendKeys(bizPincodeInput_workDet,businessAddressPincode, WaitStrategy.CLICKABLE);
        return this;
    }

    private final By bizPlaceOwnershipLabel_workDet= By.xpath("//*[@id='addressType']/label");

    private final By bizPlaceOwnershipInput_workDet= By.xpath("//*[@id='addressType']/div[1]");

    private final By bizPlaceOwnershipInput_owned= By.xpath("//*[@id='addressType']/div[1]/div[3]/ul/li[@class='multiselect__element']/span[span='Owned']/span");

    private final By bizPlaceOwnershipInput_rented= By.xpath("//*[@id='addressType']/div[1]/div[3]/ul/li[@class='multiselect__element']/span[span='Rented']/span");

    private final By bizPlaceOwnershipInput_govtSubsidy= By.xpath("//*[@id='addressType']/div[1]/div[3]/ul/li[@class='multiselect__element']/span[span='Govt subsidy']/span");

    private final By bizPlaceOwnershipInput_others= By.xpath("//*[@id='addressType']/div[1]/div[3]/ul/li[@class='multiselect__element']/span[span='Others']/span");

    public  SlushWorkDetailsScreen selectBusinessPlaceOwnership(String businessPlaceOwnership){
        click(bizPlaceOwnershipInput_workDet, WaitStrategy.CLICKABLE);
        if (businessPlaceOwnership.equalsIgnoreCase("Owned")){
            click(bizPlaceOwnershipInput_owned,WaitStrategy.PRESENCE);
        }else if (businessPlaceOwnership.equalsIgnoreCase("Rented")){
            click(bizPlaceOwnershipInput_rented,WaitStrategy.PRESENCE);
        }else if (businessPlaceOwnership.equalsIgnoreCase("Govt subsidy")){
            click(bizPlaceOwnershipInput_govtSubsidy,WaitStrategy.PRESENCE);
        }else if (businessPlaceOwnership.equalsIgnoreCase("Others")){
            click(bizPlaceOwnershipInput_others,WaitStrategy.PRESENCE);
        }else
            System.out.printf("Please provide business ownership type");
        return this;
    }

    private final By bizYearsLabel_workDet= By.xpath("//*[@id='incomeInput']/label");

    private final By bizYearsList_workDet= By.xpath("//*[@id='incomeInput']");
    private final By bizYearsList_div= By.xpath("//*[@id='incomeInput']");

    private final By bizYearsList_1year= By.xpath("//*[@id='yearBlock']/div/div[3]/ul/li[1]/span/span");

    private final By bizYearsList_2year= By.xpath("//*[@id='yearBlock']/div/div[3]/ul/li[2]/span/span");

    private final By bizYearsList_3year= By.xpath("//*[@id='yearBlock']/div/div[3]/ul/li[3]/span/span");

    private final By bizYearsList_4year= By.xpath("//*[@id='yearBlock']/div/div[3]/ul/li[4]/span/span");

    private final By bizYearsList_5year= By.xpath("//*[@id='yearBlock']/div/div[3]/ul/li[5]/span/span");

    public  SlushWorkDetailsScreen selectHowOldIsBusiness(String howOldIsBusiness){
        click(bizYearsList_workDet, WaitStrategy.CLICKABLE);
        if (howOldIsBusiness.equalsIgnoreCase("1 year")){
            click(bizYearsList_1year,WaitStrategy.PRESENCE);
        }else if (howOldIsBusiness.equalsIgnoreCase("2 years")){
            click(bizYearsList_2year,WaitStrategy.PRESENCE);
        }else if (howOldIsBusiness.equalsIgnoreCase("3 years")){
            click(bizYearsList_3year,WaitStrategy.PRESENCE);
        }else if (howOldIsBusiness.equalsIgnoreCase("4 years")){
            click(bizYearsList_4year,WaitStrategy.PRESENCE);
        }else if (howOldIsBusiness.equalsIgnoreCase("5+ years")){
            click(bizYearsList_5year,WaitStrategy.PRESENCE);
        }
        else
            System.out.printf("Please provide how old is your business");
        return this;
    }

    private final By itrFiledLabel_workDet= By.xpath("//*[@id='itrFiledInput']/label");

    private final By itrFiledYes_workDet= By.xpath("//*[@id='itrFiledInput']/div[1]/label[1]");

    private final By itrFiledNo_workDet= By.xpath("//*[@id='itrFiledInput']/div[1]/label[2]");

    public  SlushWorkDetailsScreen selectItrFiled(String isItrFiled){
        if (isItrFiled.equalsIgnoreCase("yes")){
            click(itrFiledYes_workDet,WaitStrategy.PRESENCE);
        }else if (isItrFiled.equalsIgnoreCase("no")){
            click(itrFiledNo_workDet,WaitStrategy.PRESENCE);
        }else
            System.out.printf("Please select if ITR is field?");
        return this;
    }

    private final By gstFiledLabel_workDet= By.xpath("//*[@id='gstFiledInput']/label");

    private final By gstFiledYes_workDet= By.xpath("//*[@id='gstFiledInput']/div[1]/label[1]");

    private final By gstFiledNo_workDet= By.xpath("//*[@id='gstFiledInput']/div[1]/label[2]");

    public  SlushWorkDetailsScreen selectGstFiled(String isGstFiled){
        if (isGstFiled.equalsIgnoreCase("yes")){
            click(gstFiledYes_workDet,WaitStrategy.PRESENCE);
        }else if (isGstFiled.equalsIgnoreCase("no")){
            click(gstFiledNo_workDet,WaitStrategy.PRESENCE);
        }else
            System.out.printf("Please select if GST is field?");
        return this;
    }

    private final By bureauCheckbox_workDet= By.xpath("//*[@id='checker']/label/span");

    public  SlushWorkDetailsScreen provideBureauConsent(){
        click(bureauCheckbox_workDet, WaitStrategy.CLICKABLE);
        return this;
    }

    private final By bureauInfoLink_workDet= By.xpath("//*[@id='checker']/div[1]/p/span");

    private final By bureauInfoOverlayClose_workDet= By.xpath("//*[@id='loanModal']/div[2]/div[1]");

    private final By submitWorkDetails_workDet= By.xpath("//*[@type='submit']");

    public  SlushWorkDetailsScreen submitWorkDetails(){
        click(submitWorkDetails_workDet, WaitStrategy.CLICKABLE);
        return this;
    }

//error messages of all field - Self Employed

    private final By bizNameError_workDet= By.xpath("//*[@id='businessNameInput']/div[1]");

    private final By bizAddError_workDet= By.xpath("//*[@id='businessAddressInput']/div[1]");


    private final By bizPincodeError_workDet= By.xpath("//*[@id='businessPincodeInput']/div[1]");

    private final By bizPlaceError_workDet= By.xpath("//*[@id='addressType']/div[2]");

    private final By bizYearsError_workDet= By.xpath("//*[@id='incomeInput']/div[2]");

    private final By itrFiledError_workDet= By.xpath("//*[@id='itrFiledInput']/div[2]");

    private final By gstFiledError_workDet= By.xpath("//*[@id='gstFiledInput']/div[2]");

    private final By bureauError_workDet= By.xpath("//*[@id='checker']/div[2]");

//Salaried Profile

    private final By currentCompNameLabel_workDet= By.xpath("//*[@id='currentCompanyNameInput']/label");

    private final By currentCompNameInput_workDet= By.xpath("//*[@id='currentCompanyName']");

    public  SlushWorkDetailsScreen enterCurrentCompanyName(String currentCompanyName){
        sendKeys(currentCompNameInput_workDet,currentCompanyName, WaitStrategy.CLICKABLE);
        return this;
    }

    private final By workEmailLabel_workDet= By.xpath("//*[@id='workEmailIdInput']/label");

    private final By workEmailInput_workDet= By.xpath("//*[@id='workEmailId']");

    public  SlushWorkDetailsScreen enterWorkEmail(String workEmail){
        sendKeys(workEmailInput_workDet,workEmail, WaitStrategy.CLICKABLE);
        return this;
    }

    private final By officePincodeLabel_workDet= By.xpath("//*[@id='officePincodeInput']/label");

    private final By officePincodeInput_workDet= By.xpath("//*[@id='officePincode']");

    public  SlushWorkDetailsScreen enterOfficeAddressPincode(String  officeAddressPincode){
        sendKeys(officePincodeInput_workDet,officeAddressPincode, WaitStrategy.CLICKABLE);
        return this;
    }

    private final By workExpYear_workDet= By.xpath("//*[@id='yearBlock']/div/div[2]/span");

    private final By workExpYear_0year= By.xpath("//*[@id='yearBlock']/div/div[3]/ul/li[@class='multiselect__element']/span[span='0']/span");
    private final By workExpYear_1year= By.xpath("//*[@id='yearBlock']/div/div[3]/ul/li[@class='multiselect__element']/span[span='1 year']/span");
    private final By workExpYear_2year= By.xpath("//*[@id='yearBlock']/div/div[3]/ul/li[@class='multiselect__element']/span[span='2 years']/span");
    private final By workExpYear_3year= By.xpath("//*[@id='yearBlock']/div/div[3]/ul/li[@class='multiselect__element']/span[span='3 years']/span");
    private final By workExpYear_4year= By.xpath("//*[@id='yearBlock']/div/div[3]/ul/li[@class='multiselect__element']/span[span='4 years']/span");
    private final By workExpYear_5year= By.xpath("//*[@id='yearBlock']/div/div[3]/ul/li[@class='multiselect__element']/span[span='5+ years']/span");


    private final By workExpMonth_workDet= By.xpath("//*[@id='monthBlock']/div/div[2]/span");

    private final By workExpMonth_0month= By.xpath("//*[@id='monthBlock']/div/div[3]/ul/li[@class='multiselect__element']/span/span[./text()='0']");
    private final By workExpMonth_1month= By.xpath("//*[@id='monthBlock']/div/div[3]/ul/li[@class='multiselect__element']/span/span[./text()='1 month']");
    private final By workExpMonth_2month= By.xpath("//*[@id='monthBlock']/div/div[3]/ul/li[@class='multiselect__element']/span/span[./text()='2 months']");
    private final By workExpMonth_3month= By.xpath("//*[@id='monthBlock']/div/div[3]/ul/li[@class='multiselect__element']/span/span[./text()='3 months']");
    private final By workExpMonth_4month= By.xpath("//*[@id='monthBlock']/div/div[3]/ul/li[@class='multiselect__element']/span/span[./text()='4 months']");
    private final By workExpMonth_5month= By.xpath("//*[@id='monthBlock']/div/div[3]/ul/li[@class='multiselect__element']/span/span[./text()='5 months']");
    private final By workExpMonth_6month= By.xpath("//*[@id='monthBlock']/div/div[3]/ul/li[@class='multiselect__element']/span/span[./text()='6 months']");
    private final By workExpMonth_7month= By.xpath("//*[@id='monthBlock']/div/div[3]/ul/li[@class='multiselect__element']/span/span[./text()='7 months']");
    private final By workExpMonth_8month= By.xpath("//*[@id='monthBlock']/div/div[3]/ul/li[@class='multiselect__element']/span/span[./text()='8 months']");
    private final By workExpMonth_9month= By.xpath("//*[@id='monthBlock']/div/div[3]/ul/li[@class='multiselect__element']/span/span[./text()='9 months']");
    private final By workExpMonth_10month= By.xpath("//*[@id='monthBlock']/div/div[3]/ul/li[@class='multiselect__element']/span/span[./text()='10 months']");
    private final By workExpMonth_11month= By.xpath("//*[@id='monthBlock']/div/div[3]/ul/li[@class='multiselect__element']/span/span[./text()='11 months']");


    public  SlushWorkDetailsScreen selectWorkingSinceInCurrentOrganization(String years, String months){
        click(workExpYear_workDet, WaitStrategy.CLICKABLE);
        if (years.equalsIgnoreCase("0")){
            click(workExpYear_0year,WaitStrategy.PRESENCE);
        }else if (years.equalsIgnoreCase("1 year")){
            click(workExpYear_1year,WaitStrategy.PRESENCE);
        }else if (years.equalsIgnoreCase("2 years")){
            click(workExpYear_2year,WaitStrategy.PRESENCE);
        }else if (years.equalsIgnoreCase("3 years")){
            click(workExpYear_3year,WaitStrategy.PRESENCE);
        }else if (years.equalsIgnoreCase("4 years")){
            click(workExpYear_4year,WaitStrategy.PRESENCE);
        }else if (years.equalsIgnoreCase("5+ years")){
            click(workExpYear_5year,WaitStrategy.PRESENCE);
        }
        else
            System.out.printf("Please provide how old is your business");

        click(workExpMonth_workDet, WaitStrategy.CLICKABLE);
        if (months.equalsIgnoreCase("0")){
            click(workExpMonth_0month,WaitStrategy.PRESENCE);
        }else if (months.equalsIgnoreCase("1 month")){
            click(workExpMonth_1month,WaitStrategy.PRESENCE);
        }else if (months.equalsIgnoreCase("2 months")){
            click(workExpMonth_2month,WaitStrategy.PRESENCE);
        }else if (months.equalsIgnoreCase("3 months")){
            click(workExpMonth_3month,WaitStrategy.PRESENCE);
        }else if (months.equalsIgnoreCase("4 months")){
            click(workExpMonth_4month,WaitStrategy.PRESENCE);
        }else if (months.equalsIgnoreCase("5 months")){
            click(workExpMonth_5month,WaitStrategy.PRESENCE);
        }else if (months.equalsIgnoreCase("6 months")){
            click(workExpMonth_6month,WaitStrategy.PRESENCE);
        }else if (months.equalsIgnoreCase("7 months")){
            click(workExpMonth_7month,WaitStrategy.PRESENCE);
        }else if (months.equalsIgnoreCase("8 months")){
            click(workExpMonth_8month,WaitStrategy.PRESENCE);
        }else if (months.equalsIgnoreCase("9 months")){
            click(workExpMonth_9month,WaitStrategy.PRESENCE);
        }else if (months.equalsIgnoreCase("10 months")){
            click(workExpMonth_10month,WaitStrategy.PRESENCE);
        }else if (months.equalsIgnoreCase("11 months")){
            click(workExpMonth_11month,WaitStrategy.PRESENCE);
        }
        else
            System.out.printf("Please provide working since in current company in Years and Months");
        return this;
    }


//error messages of all field - Salaried

    private final By currentCompanyError_workDet= By.xpath("//*[@id='currentCompanyNameInput']/div[1]");

    private final By workEmailError_workDet= By.xpath("//*[@id='workEmailIdInput']/div[1]");

    private final By officePincodeError_workDet= By.xpath("//*[@id='officePincodeInput']/div[1]");

    private final By workExpError_workDet= By.xpath("//*[@id='incomeInput']/div[2]");

}
