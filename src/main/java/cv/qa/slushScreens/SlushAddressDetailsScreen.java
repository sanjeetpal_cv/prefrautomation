package cv.qa.slushScreens;

import cv.qa.base.UiBase;
import cv.qa.enums.WaitStrategy;
import org.openqa.selenium.By;

public class SlushAddressDetailsScreen extends UiBase {

    private final By screenName_addDet= By.xpath("//*[@class='cvLoanHeaderCol']");

    private final By currAddTypeLabel_addDet= By.xpath("//*[@id='addressTypeInput']/label");

    private final By currAddTypeList_addDet= By.xpath("//*[@id='addressTypeInput']/div[1]/div[2]/span[ contains(text(),'Select an option')]");

    private final By currAddTypeList_owned= By.xpath("//*[@id='addressTypeInput']/div[1]/div[3]/ul/li[@class='multiselect__element']/span[span='Owned/Parental Owned']/span");

    private final By currAddTypeList_div= By.xpath("//*[@id='addressTypeInput']/div[1]/div[3]");

    private final By currAddTypeList_rented= By.xpath("//*[@id='addressTypeInput']/div[1]/div[3]/ul/li[@class='multiselect__element']/span[span='Rented']/span");

    private final By currAddTypeList_bachelor= By.xpath("//*[@id='addressTypeInput']/div[1]/div[3]/ul/li[@class='multiselect__element']/span[span='PG/Bachelor accommodation']/span");

    private final By currAddTypeList_hostel= By.xpath("//*[@id='addressTypeInput']/div[1]/div[3]/ul/li[@class='multiselect__element']/span[span='Hostel']/span");

    private final By currAddTypeList_trainingCamp= By.xpath("//*[@id='addressTypeInput']/div[1]/div[3]/ul/li[@class='multiselect__element']/span[span='Training camp']/span");


    public  SlushAddressDetailsScreen selectCurrentAddressType(String currentAddressType){
        click(currAddTypeList_addDet, WaitStrategy.CLICKABLE);
        click(currAddTypeList_div,WaitStrategy.PRESENCE);
        if (currentAddressType.equalsIgnoreCase("Owned/Parental Owned")){
            click(currAddTypeList_owned,WaitStrategy.PRESENCE);
        }else if (currentAddressType.equalsIgnoreCase("Rented")){
            click(currAddTypeList_rented,WaitStrategy.PRESENCE);
        }else if (currentAddressType.equalsIgnoreCase("PG/Bachelor accommodation")){
            click(currAddTypeList_bachelor,WaitStrategy.PRESENCE);
        }else if (currentAddressType.equalsIgnoreCase("Hostel")){
            click(currAddTypeList_hostel,WaitStrategy.PRESENCE);
        }else if (currentAddressType.equalsIgnoreCase("Training camp")){
            click(currAddTypeList_trainingCamp,WaitStrategy.PRESENCE);
        }
            System.out.printf("Please provide purpose of loan");
        return this;
    }

    private final By currAddInputLabel_addDet= By.xpath("//*[@id='addressInput']/label");

    private final By currAddInput_addDet= By.xpath("//*[@id='address']");

    public  SlushAddressDetailsScreen enterCurrentAddress(String currentAddress){
        sendKeys(currAddInput_addDet,currentAddress,WaitStrategy.CLICKABLE);
        return this;
    }

    private final By currPincodeLabel_addDet= By.xpath("//*[@id='pincodeInput']/label");

    private final By currPincode_addDet= By.xpath("//*[@id='pincode']");

    public  SlushAddressDetailsScreen enterCurrentAddressPincode(String currentAddressPincode){
        sendKeys(currPincode_addDet,currentAddressPincode,WaitStrategy.CLICKABLE);
        return this;
    }

    private final By confirmCurrAsPermAdd_addDet= By.xpath("//*[@id='permanentAddressSameAsCurrAddressInput']/label");

    private final By addConfirmPerm_Yes_addDet= By.xpath("//*[@id=\"radioBlock\"]/label[1]");

    private final By addConfirmPerm_No_addDet= By.xpath("//*[@id=\"radioBlock\"]/label[2]");

    public  SlushAddressDetailsScreen selectIsAddressTypePermanent(String currAddTypePermanent){
        if (currAddTypePermanent.equalsIgnoreCase("yes")){
            click(addConfirmPerm_Yes_addDet,WaitStrategy.CLICKABLE);
        }else if (currAddTypePermanent.equalsIgnoreCase("no")){
            click(addConfirmPerm_No_addDet,WaitStrategy.PRESENCE);
        }
        System.out.printf("Please select current addresstype Yes/No");
        return this;
    }


    private final By submit_addDet= By.xpath("//*[@type='submit']");

    public  SlushAddressDetailsScreen submitAddressDetails(){
        click(submit_addDet,WaitStrategy.CLICKABLE);
        return this;
    }

    private final By currAddTypeErr_addDet= By.xpath("");

    private final By currAddInputErr_addDet= By.xpath("//*[@id='addressInput']/div[1]");

    private final By currAddPincodeErr_addDet= By.xpath("//*[@id='pincodeInput']/div[1]");

    private final By confirmCurrAddTypeErr_addDet= By.xpath("//*[@id='permanentAddressSameAsCurrAddressInput']/div[2]");

}
