package cv.qa.slushScreens;

import cv.qa.base.UiBase;
import cv.qa.enums.WaitStrategy;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;

public class SlushUploadDocumentsScreen extends UiBase {

    //Launch screen or introduction screen
    private final By screenName_UpDoc= By.xpath("//*[@class='cvLoanHeaderTitle' and contains(text(),'Upload documents')]");
    private final By docList_UpDoc= By.xpath("//*[@class='stepCounter-list']");
    private final By docListSelfie_UpDoc= By.xpath("//*[@id='noCustomHeader']//*[@class='cameraSvg']");
    private final By docListPan_UpDoc= By.xpath("//*[@class='stepCounter-list']/li[2]/span[1]");
    private final By docListKyc_UpDoc= By.xpath("//*[@class='stepCounter-list']/li[3]/span[1]");

    //Proceed after uploading document
    private final By proceed_UploadDocButton= By.xpath("//*[@id='uploadDocumentsSubmitButton']");

    public  SlushUploadDocumentsScreen proceedUploadingDocuments(){
        click(proceed_UploadDocButton, WaitStrategy.CLICKABLE);
        return this;
    }

//Photo_proof or Selfie Screen
    private final By selfieScreenName_UpDoc= By.xpath("//*[@class='uploadCardPlaceholder']");
    private final By selfie_upload_location= By.xpath("//*[@id='files_photoProof']");

    public  SlushUploadDocumentsScreen uploadSelfie(String fileNameWithExtension){
        uploadFileSendKeys(selfie_upload_location,fileNameWithExtension,WaitStrategy.PRESENCE);
        return this;
    }

//Current Address same as KYC address

    private final By currentAddressSameAsKyc_header= By.xpath("//*[@class='cvLoanHeaderTitle' and contains(text(),'Current Address Check')]");

    private final By currentAddressSameAsKyc_yes= By.xpath("//*[@id='radioBlock']/label[contains(text(),'Yes')]");
    private final By currentAddressSameAsKyc_no= By.xpath("//*[@id='radioBlock']/label[contains(text(),'No')]");

    public  SlushUploadDocumentsScreen confirmCurrentAddressSameAsKyc(String currentAddressSameAsKyc){
        if (currentAddressSameAsKyc.equalsIgnoreCase("yes")){
            click(currentAddressSameAsKyc_yes,WaitStrategy.PRESENCE);
        }else if (currentAddressSameAsKyc.equalsIgnoreCase("no")){
            click(currentAddressSameAsKyc_no,WaitStrategy.PRESENCE);
        }
        return this;
    }

    private final By proceedButton_currentAddress= By.xpath("//*[@id='uploadDocumentsSubmitButton' and contains(text(),'PROCEED')]");

    public  SlushUploadDocumentsScreen proceedFromCurrentAddress(){
        click(proceedButton_currentAddress, WaitStrategy.CLICKABLE);
        return this;
    }

    private final By currentAddressProof_List= By.xpath("//*[@id='dropdown']");
    private final By currentAddressProof_electricityBill= By.xpath("//*[@id='dropdown']/option[contains(text(),'Electricity bill')]");
    private final By currentAddressProof_gasBill= By.xpath("//*[@id='dropdown']/option[contains(text(),'Gas bill')]");
    private final By currentAddressProof_postpaidMobileBill= By.xpath("//*[@id='dropdown']/option[contains(text(),'Postpaid Mobile Bill')]");
    private final By currentAddressProof_postpaidLandlineBill= By.xpath("//*[@id='dropdown']/option[contains(text(),'Postpaid Landline Bill')]");
    private final By currentAddressProof_voterId= By.xpath("//*[@id='dropdown']/option[contains(text(),'VoterID')]");
    private final By currentAddressProof_drivingLicense= By.xpath("//*[@id='dropdown']/option[contains(text(),'Driving License')]");
    private final By currentAddressProof_waterBill= By.xpath("//*[@id='dropdown']/option[contains(text(),'Water bill')]");
    private final By currentAddressProof_bankStatement= By.xpath("//*[@id='dropdown']/option[contains(text(),'Bank Statement')]");
    private final By currentAddressProof_bankPassbook= By.xpath("//*[@id='dropdown']/option[contains(text(),'Bank Passbook')]");

    public  SlushUploadDocumentsScreen selectCurrentAddressProofType(String currentAddressProof){
        if (currentAddressProof.equalsIgnoreCase("Electricity bill")){
            click(currentAddressProof_electricityBill,WaitStrategy.PRESENCE);
        }else if (currentAddressProof.equalsIgnoreCase("Gas bill")){
            click(currentAddressProof_gasBill,WaitStrategy.PRESENCE);
        }else if (currentAddressProof.equalsIgnoreCase("Postpaid Mobile Bill")){
            click(currentAddressProof_postpaidMobileBill,WaitStrategy.PRESENCE);
        }else if (currentAddressProof.equalsIgnoreCase("Postpaid Landline Bill")){
            click(currentAddressProof_postpaidLandlineBill,WaitStrategy.PRESENCE);
        }else if (currentAddressProof.equalsIgnoreCase("VoterID")){
            click(currentAddressProof_voterId,WaitStrategy.PRESENCE);
        }else if (currentAddressProof.equalsIgnoreCase("Driving License")){
            click(currentAddressProof_drivingLicense,WaitStrategy.PRESENCE);
        }else if (currentAddressProof.equalsIgnoreCase("Water bill")){
            click(currentAddressProof_waterBill,WaitStrategy.PRESENCE);
        }else if (currentAddressProof.equalsIgnoreCase("Bank Statement")){
            click(currentAddressProof_bankStatement,WaitStrategy.PRESENCE);
        }else if (currentAddressProof.equalsIgnoreCase("Bank Passbook")){
            click(currentAddressProof_bankPassbook,WaitStrategy.PRESENCE);
        }else
            System.out.printf("Please provide current address type");
        return this;
    }

    private final By currentAddressProof_upload= By.xpath("//*[@id='files_addressProof']");

    public  SlushUploadDocumentsScreen uploadCurrentAddressProof(String fileNameWithExtension){
        uploadFileSendKeys(currentAddressProof_upload,fileNameWithExtension,WaitStrategy.PRESENCE);
        return this;
    }

    private final By nachProof_upload= By.xpath("//*[@id='files_nach_proof']");
    private final By nachProof_passwordProtectedCheckmark= By.xpath("//*[@class='passwordCheckBlock']/label/span[@class='checkmark']");
    private final By nachProof_passwordInput= By.xpath("//*[@class='cv-input-field']/input[@placeholder='Enter Password Here']");


    public  SlushUploadDocumentsScreen uploadNachProof(String fileNameWithExtension){
        uploadFileRobotClass(nachProof_upload,fileNameWithExtension,WaitStrategy.PRESENCE);
        click(proceed_UploadDocButton,WaitStrategy.CLICKABLE);
        return this;
    }

    public  SlushUploadDocumentsScreen uploadNachProof(String fileNameWithExtension, String password){
        uploadFileSendKeys(nachProof_upload,fileNameWithExtension,WaitStrategy.PRESENCE);
        click(nachProof_passwordProtectedCheckmark,WaitStrategy.CLICKABLE);
        sendKeys(nachProof_passwordInput,password,WaitStrategy.VISIBLE);
        click(proceed_UploadDocButton,WaitStrategy.CLICKABLE);
        return this;
    }



}
