package cv.qa.slushScreens;

import cv.qa.base.UiBase;
import cv.qa.enums.WaitStrategy;
import org.openqa.selenium.By;

public class SlushCreditBureau extends UiBase {

    private final By cibil_questionSubTitle= By.xpath("//*[@class='cvLoanHeaderSubTitle'  and contains(text(),'Please answer the following questions for us to pull your CIBIL report successfully')]");

    private final By cibil_questionSubmit= By.xpath("//button[@type='submit' and contains(text(),'PROCEED')]");

    public SlushCreditBureau selectAnswerOptions_stub(int questionNo,int answerNo) {
        for (int i=1; i <= 5; i++){
            final By cibil_answerOption = By.xpath("//*[@class='card questionnarieCard']/div/div[" + questionNo + "]/div[2]/div[" + answerNo + "]");
            click(cibil_answerOption, WaitStrategy.CLICKABLE);
            questionNo++;
            answerNo++;
            }
        click(cibil_questionSubmit,WaitStrategy.CLICKABLE);
            return this;
        }


    }
