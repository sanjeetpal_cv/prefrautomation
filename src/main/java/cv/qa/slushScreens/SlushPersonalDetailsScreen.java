package cv.qa.slushScreens;

import cv.qa.base.UiBase;
import cv.qa.drivers.WebDriverManager;
import cv.qa.enums.WaitStrategy;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;

public class SlushPersonalDetailsScreen extends UiBase {

    private final By  screenName_perDet= By.xpath("//*[@class='cvLoanHeaderCol']");

    private final By  fNameLabel_perDet= By.xpath("//*[@id='firstNameInput']/label");

    private final By  fNameInput_perDet= By.xpath("//*[@id='firstName']");

    public  SlushPersonalDetailsScreen enterFirstName(String firstName){
        sendKeys(fNameInput_perDet,firstName, WaitStrategy.PRESENCE);
        return this;
    }

    private final By  fNameInfo_perDet= By.xpath("//*[@id='firstNameInput']/div[1]");

    private final By  lNameLabel_perDet= By.xpath("//*[@id='lastNameInput']/label");

    private final By  lNameInput_perDet= By.xpath("//*[@id='lastName']");

    public  SlushPersonalDetailsScreen enterLastName(String lastName){
        sendKeys(lNameInput_perDet,lastName, WaitStrategy.PRESENCE);
        return this;
    }

    private final By lNameInfo_perDet = By.xpath("//*[@id='lastNameInput']/div[1]");

    private final By  motherNameLabel_perDet= By.xpath("//*[@id='motherNameInput']/label");

    private final By motherNameInput_perDet = By.xpath("//*[@id='motherName']");

    public  SlushPersonalDetailsScreen enterMothersName(String motherName){
        sendKeys(motherNameInput_perDet,motherName, WaitStrategy.PRESENCE);
        return this;
    }

    private final By  genderLabel_perDet= By.xpath("//*[@id='genderInput']/label");

    private final By  genderMale_mBtn_perDet= By.xpath("//*[@id='radioBlock']/label[1]/span");

    private final By  genderMale_fBtn_perDet= By.xpath("//*[@id='radioBlock']/label[2]/span");

    public  SlushPersonalDetailsScreen selectGender(String gender){
        if (gender.equalsIgnoreCase("male")){
            click(genderMale_mBtn_perDet,WaitStrategy.CLICKABLE);
        }else if (gender.equalsIgnoreCase("female")){
            click(genderMale_fBtn_perDet,WaitStrategy.CLICKABLE);
        }else
            System.out.printf("Please provide gender");
        return this;
    }

    private final By dobLabel_perDet = By.xpath("//*[@id='dobInput']/label");

    private final By dobYear_dropdown=By.xpath("//*[@id='yearBlock']/div/div[2]/span");
    private final By dobYear_input=By.xpath("//input[@id='yearDropdown']");
    private final By dobYear_dropdownDiv=By.xpath("//*[@id='yearBlock']/div/div[3]");

    private final By dobMonth_dropdown=By.xpath("//*[@id='monthBlock']/div/div[2]/span");
    private final By dobMonth_input=By.xpath("//input[@id='monthDropdown']");
    private final By dobMonth_dropdownDiv=By.xpath("//*[@id='monthBlock']/div/div[3]");


    private final By dobDate_dropdown=By.xpath("//*[@id='dateBlock']/div/div[2]/span");
    private final By dobDate_input= By.xpath("//input[@id='dateDropdown']");
    private final By dobDate_dropdownDiv=By.xpath("//*[@id='dateBlock']/div/div[3]");


    public SlushPersonalDetailsScreen selectDob(String year, String month, String date){
        click(dobYear_dropdown,WaitStrategy.CLICKABLE);
        sendKeys(dobYear_input,year,WaitStrategy.CLICKABLE);
        click(dobYear_dropdownDiv,WaitStrategy.PRESENCE);

        click(dobMonth_dropdown,WaitStrategy.CLICKABLE);
        sendKeys(dobMonth_input,month,WaitStrategy.CLICKABLE);
        click(dobMonth_dropdownDiv,WaitStrategy.PRESENCE);

        click(dobDate_dropdown,WaitStrategy.CLICKABLE);
        sendKeys(dobDate_input,date,WaitStrategy.CLICKABLE);
        click(dobDate_dropdownDiv,WaitStrategy.PRESENCE);
        return this;
    }

    private final By personalEmailLabel_perDet = By.xpath("//*[@id='personalEmailIdInput']/label");

    private final By  personalEmailInput_perDet= By.xpath("//*[@id='personalEmailId']");

    public  SlushPersonalDetailsScreen enterPersonalEmail(String personalEmail){
        sendKeys(personalEmailInput_perDet,personalEmail, WaitStrategy.PRESENCE);
        return this;
    }

    private final By  panNumberLabel_perDet= By.xpath("//*[@id='panNumberInput']/label");

    private final By panNumberInput_perDet = By.xpath("//*[@id='panNumber']");

    public  SlushPersonalDetailsScreen enterPanNumber(String panNumber){
        sendKeys(panNumberInput_perDet,panNumber, WaitStrategy.PRESENCE);
        return this;
    }

    private final By panNumberInfo_perDet = By.xpath("//*[@id='panNumberInput']/label/span[2]");

    private final By panNumberInfoData_perDet = By.xpath("//*[@id='panNumberInput']/label/span[2]/span[1]/div");

    private final By  requiredLoanAmountLabel_perDet= By.xpath("//*[@id='desiredLoanAmountInput']/label");

    private final By  requiredLoanAmountInput_perDet= By.xpath("//*[@id='desiredLoanAmount']");

    public  SlushPersonalDetailsScreen enterRequiredLoanAmount(String desiredLoanAmount){
        sendKeys(requiredLoanAmountInput_perDet,desiredLoanAmount, WaitStrategy.PRESENCE);
        return this;
    }

    private final By  loanPurposeLabel_perDet= By.xpath("//*[@id='loanPurposeInput']/label");

    private final By  loanPurpose_perDet= By.xpath("//*[@id='loanPurposeInput']/div[1]/div[2]");

    private final By  loanPurpose_medical= By.xpath("//*[@id='loanPurposeInput']/div[1]/div[3]/ul/li[@class='multiselect__element']/span[span='Medical']/span");

    private final By  loanPurpose_marriage= By.xpath("//*[@id='loanPurposeInput']/div[1]/div[3]/ul/li[@class='multiselect__element']/span[span='Marriage /other ceremonies']/span");

    private final By  loanPurpose_cash= By.xpath("//*[@id='loanPurposeInput']/div[1]/div[3]/ul/li[@class='multiselect__element']/span[span='Need of cash at home']/span");

    private final By  loanPurpose_vacation= By.xpath("//*[@id='loanPurposeInput']/div[1]/div[3]/ul/li[@class='multiselect__element']/span[span='Vacation']/span");

    private final By  loanPurpose_renovation= By.xpath("//*[@id='loanPurposeInput']/div[1]/div[3]/ul/li[@class='multiselect__element']/span[span='Buying property/House renovation']/span");

    private final By loanPurpose_vehicle = By.xpath("//*[@id='loanPurposeInput']/div[1]/div[3]/ul/li[@class='multiselect__element']/span[span='Buying a vehicle']/span");

    private final By  loanPurpose_electronics= By.xpath("//*[@id='loanPurposeInput']/div[1]/div[3]/ul/li[@class='multiselect__element']/span[span='Electronics Purchase']/span");

    private final By  loanPurpose_education= By.xpath("//*[@id='loanPurposeInput']/div[1]/div[3]/ul/li[@class='multiselect__element']/span[span='Education for self/ Dependents']/span");

    private final By  loanPurpose_business= By.xpath("//*[@id='loanPurposeInput']/div[1]/div[3]/ul/li[@class='multiselect__element']/span[span='Starting / expanding business']/span");

    private final By  loanPurpose_repay= By.xpath("//*[@id='loanPurposeInput']/div[1]/div[3]/ul/li[@class='multiselect__element']/span[span='Repay other loans']/span");

    private final By  loanPurpose_others= By.xpath("//*[@id='loanPurposeInput']/div[1]/div[3]/ul/li[@class='multiselect__element']/span[span='Others']/span");

    public  SlushPersonalDetailsScreen selectPurposeOfLoan(String loanPurpose){
        click(loanPurpose_perDet,WaitStrategy.CLICKABLE);
        if (loanPurpose.equalsIgnoreCase("Medical")){
            click(loanPurpose_medical,WaitStrategy.PRESENCE);
        }else if (loanPurpose.equalsIgnoreCase("Marriage /other ceremonies")){
            click(loanPurpose_marriage,WaitStrategy.PRESENCE);
        }else if (loanPurpose.equalsIgnoreCase("Need of cash at home")){
            click(loanPurpose_cash,WaitStrategy.PRESENCE);
        }else if (loanPurpose.equalsIgnoreCase("Vacation")){
            click(loanPurpose_vacation,WaitStrategy.PRESENCE);
        }else if (loanPurpose.equalsIgnoreCase("Buying property/House renovation")){
            click(loanPurpose_renovation,WaitStrategy.PRESENCE);
        }else if (loanPurpose.equalsIgnoreCase("Buying a vehicle")){
            click(loanPurpose_vehicle,WaitStrategy.PRESENCE);
        }else if (loanPurpose.equalsIgnoreCase("Electronics Purchase")){
            click(loanPurpose_electronics,WaitStrategy.PRESENCE);
        }else if (loanPurpose.equalsIgnoreCase("Education for self/ Dependents")){
            click(loanPurpose_education,WaitStrategy.PRESENCE);
        }else if (loanPurpose.equalsIgnoreCase("Starting / expanding business")){
            click(loanPurpose_business,WaitStrategy.PRESENCE);
        }else if (loanPurpose.equalsIgnoreCase("Repay other loans")){
            click(loanPurpose_repay,WaitStrategy.PRESENCE);
        }else if (loanPurpose.equalsIgnoreCase("Others")){
            click(loanPurpose_others,WaitStrategy.PRESENCE);
        }else
            System.out.printf("Please provide purpose of loan");
        return this;
    }

    private final By  submitPersonalLoan_perDet= By.xpath("//*[@type='submit']");

    public  SlushPersonalDetailsScreen submitPersonalDetails(){
        click(submitPersonalLoan_perDet,WaitStrategy.CLICKABLE);
        return this;
    }

//All Failures

    private final By  fNameError_perDet= By.xpath("//*[@id='firstNameInput']/div[2]");

    private final By  lNameError_perDet= By.xpath("//*[@id='lastNameInput']/div[2]");

    private final By  motherNameError_perDet= By.xpath("//*[@id='motherNameInput']/div[1]");

    private final By  genderError_perDet= By.xpath("//*[@id='genderInput']/div[2]");

    private final By  dobError_perDet= By.xpath("//*[@id='dobInput']/div[2]");

    private final By personalEmailError_perDet = By.xpath("//*[@id='personalEmailIdInput']/div[1]");

    private final By  panNumberError_perDet= By.xpath("//*[@id='panNumberInput']/div[1]");

    private final By  requiredLoanAmountError_perDet= By.xpath("//*[@id='desiredLoanAmountInput']/div[1]");

    private final By  loanPurposeError_perDet= By.xpath("//*[@id='loanPurposeInput']/div[2]");

}
