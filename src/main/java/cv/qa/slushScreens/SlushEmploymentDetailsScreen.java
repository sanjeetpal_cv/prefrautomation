package cv.qa.slushScreens;

import cv.qa.base.UiBase;
import cv.qa.enums.WaitStrategy;
import org.openqa.selenium.By;


public class SlushEmploymentDetailsScreen extends UiBase {

    private final By  screenName_empDet = By.xpath("//*[@class='cvLoanHeaderCol']");
    private final By empTypeDropdown_empDet = By.xpath("//*[@id='employmentTypeInput']/div[1]/div[2]");

    private final By empTypeInput_Salaried = By.xpath("//*[@id='employmentTypeInput']/div[1]/div[3]/ul/li[@class='multiselect__element']/span[span='Salaried']");

    private final By empTypeInput_SelfEmployed= By.xpath("//*[@id='employmentTypeInput']/div[1]/div[3]/ul/li[@class='multiselect__element']/span[span='Self Employed']");

    private final By empTypeInput_Taxi=By.xpath("//*[@id='employmentTypeInput']/div[1]/div[3]/ul/li[@class='multiselect__element']/span[span='Taxi/Delivery or freelance services']");

    public  SlushEmploymentDetailsScreen selectEmpType(String empType){
        click(empTypeDropdown_empDet,WaitStrategy.CLICKABLE);
        if (empType.equalsIgnoreCase("salaried")){
            click(empTypeInput_Salaried,WaitStrategy.PRESENCE);
        }else if (empType.equalsIgnoreCase("self employed")){
            click(empTypeInput_SelfEmployed,WaitStrategy.PRESENCE);
        }else if (empType.equalsIgnoreCase("Taxi/Delivery or freelance services")){
            click(empTypeInput_Taxi,WaitStrategy.PRESENCE);
        }else
            System.out.printf("Please provide employment type");
        return this;
    }

//for employment type error message
    private final By  empTypeError_empDet= By.xpath("//*[@id='employmentTypeInput']/div[2]");


//For Organisation Type
    private final By orgTypeLabel_empDet = By.xpath("//*[@id='sourceOfIncomeInput']/label");

    private final By orgType_empDet = By.xpath("//*[@id='sourceOfIncomeInput']/div[1]");

    private final By orgTypeError_empDet = By.xpath("//*[@id='sourceOfIncomeInput']/div[2]");

    private final By orgTypeDropdown_empDet = By.xpath("//*[@id='sourceOfIncomeInput']/div[1]/div[2]");

    private final By  orgTypeDropdown_Manufacturing= By.xpath("//*[@id='sourceOfIncomeInput']/div[1]/div[3]/ul/li[@class='multiselect__element']/span[span='Manufacturing']/span");

    private final By  orgTypeDropdown_IT_Services= By.xpath("//*[@id='sourceOfIncomeInput']/div[1]/div[3]/ul/li[@class='multiselect__element']/span[span='IT Services']/span");

    private final By  orgTypeDropdown_Financial_Services= By.xpath("//*[@id='sourceOfIncomeInput']/div[1]/div[3]/ul/li[@class='multiselect__element']/span[span='Financial Services']/span");

    private final By orgTypeDropdown_BPO = By.xpath("//*[@id='sourceOfIncomeInput']/div[1]/div[3]/ul/li[@class='multiselect__element']/span[span='BPO']/span");

    private final By orgTypeDropdown_Educational_services = By.xpath("//*[@id='sourceOfIncomeInput']/div[1]/div[3]/ul/li[@class='multiselect__element']/span[span='Educational services']/span");

    private final By orgTypeDropdown_Public_sector_company = By.xpath("//*[@id='sourceOfIncomeInput']/div[1]/div[3]/ul/li[@class='multiselect__element']/span[span='Public sector company']/span");

    private final By  orgTypeDropdown_Logistics_or_Automobiles= By.xpath("//*[@id='sourceOfIncomeInput']/div[1]/div[3]/ul/li[@class='multiselect__element']/span[span='Logistics/Automobiles']/span");

    private final By  orgTypeDropdown_Healthcare= By.xpath("//*[@id='sourceOfIncomeInput']/div[1]/div[3]/ul/li[@class='multiselect__element']/span[span='Healthcare']/span");

    private final By orgTypeDropdown_Others= By.xpath("//*[@id='sourceOfIncomeInput']/div[1]/div[3]/ul/li[@class='multiselect__element']/span[span='Others']/span");

    public  SlushEmploymentDetailsScreen selectOrgType(String orgType){
        click(orgTypeDropdown_empDet,WaitStrategy.CLICKABLE);
        if (orgType.equalsIgnoreCase("Manufacturing")){
            click(orgTypeDropdown_Manufacturing,WaitStrategy.PRESENCE);
        }else if (orgType.equalsIgnoreCase("IT Services")){
            click(orgTypeDropdown_IT_Services,WaitStrategy.PRESENCE);
        }else if (orgType.equalsIgnoreCase("Financial Services")){
            click(orgTypeDropdown_Financial_Services,WaitStrategy.PRESENCE);
        }else if (orgType.equalsIgnoreCase("BPO")){
            click(orgTypeDropdown_BPO,WaitStrategy.PRESENCE);
        }else if (orgType.equalsIgnoreCase("Educational services")){
            click(orgTypeDropdown_Educational_services,WaitStrategy.PRESENCE);
        }else if (orgType.equalsIgnoreCase("Public sector company")){
            click(orgTypeDropdown_Public_sector_company,WaitStrategy.PRESENCE);
        }else if (orgType.equalsIgnoreCase("Logistics/Automobiles")){
            click(orgTypeDropdown_Logistics_or_Automobiles,WaitStrategy.PRESENCE);
        }else if (orgType.equalsIgnoreCase("Healthcare")){
            click(orgTypeDropdown_Healthcare,WaitStrategy.PRESENCE);
        }else if (orgType.equalsIgnoreCase("Others")){
            click(orgTypeDropdown_Others,WaitStrategy.PRESENCE);
        }else
            System.out.printf("Please provide organization type");
        return this;
    }

    //For Business Type

    private final By  bizTypeLabel_empDet= By.xpath("//*[@id='sourceOfIncomeInput']/label");

    private final By  bizType_empDet= By.xpath("//*[@id='sourceOfIncomeInput']/div[1]/div[2]");

    private final By  bizTypeError_empDet= By.xpath("//*[@id='sourceOfIncomeInput']/div[2]");

    private final By  bizTypDropdown_Eatery= By.xpath("//*[@id='sourceOfIncomeInput']/div[1]/div[3]/ul/li[1]/span[span='Eatery/Juice shops/Pan shops']/span");

    private final By  bizTypDropdown_Kirana= By.xpath("//*[@id='sourceOfIncomeInput']/div[1]/div[3]/ul/li[@class='multiselect__element']/span[span='Kirana, Grocery or Provision Store']/span");

    private final By  bizTypDropdown_Event_organizers= By.xpath("//*[@id='sourceOfIncomeInput']/div[1]/div[3]/ul/li[@class='multiselect__element']/span[span='Event organizers']/span");

    private final By  bizTypDropdown_Travel= By.xpath("//*[@id='sourceOfIncomeInput']/div[1]/div[3]/ul/li[@class='multiselect__element']/span[span='Travel / passport / PAN card/ RTO agents']/span");

    private final By  bizTypDropdown_Carpentry= By.xpath("//*[@id='sourceOfIncomeInput']/div[1]/div[3]/ul/li[@class='multiselect__element']/span[span='Carpentry / Plumber/Electricians']/span");

    private final By  bizTypDropdown_Apparel= By.xpath("//*[@id='sourceOfIncomeInput']/div[1]/div[3]/ul/li[@class='multiselect__element']/span[span='Apparel store']/span");

    private final By  bizTypDropdown_Electronics= By.xpath("//*[@id='sourceOfIncomeInput']/div[1]/div[3]/ul/li[@class='multiselect__element']/span[span='Electronics/Mobile shops']/span");

    private final By  bizTypDropdown_Medical= By.xpath("//*[@id='sourceOfIncomeInput']/div[1]/div[3]/ul/li[@class='multiselect__element']/span[span='Medical Store/ Local Pharmacy']/span");

    private final By  bizTypDropdown_Others= By.xpath("//*[@id='sourceOfIncomeInput']/div[1]/div[3]/ul/li[@class='multiselect__element']/span[span='Others']/span");

    public  SlushEmploymentDetailsScreen selectBizType(String bizType){
        click(bizType_empDet,WaitStrategy.CLICKABLE);
        if (bizType.equalsIgnoreCase("Eatery/Juice shops/Pan shops")){
            click(bizTypDropdown_Eatery,WaitStrategy.PRESENCE);
        }else if (bizType.equalsIgnoreCase("Kirana, Grocery or Provision Store")){
            click(bizTypDropdown_Kirana,WaitStrategy.PRESENCE);
        }else if (bizType.equalsIgnoreCase("Event organizers")){
            click(bizTypDropdown_Event_organizers,WaitStrategy.PRESENCE);
        }else if (bizType.equalsIgnoreCase("Travel / passport / PAN card/ RTO agents")){
            click(bizTypDropdown_Travel,WaitStrategy.PRESENCE);
        }else if (bizType.equalsIgnoreCase("Carpentry / Plumber/Electricians")){
            click(bizTypDropdown_Carpentry,WaitStrategy.PRESENCE);
        }else if (bizType.equalsIgnoreCase("Apparel store")){
            click(bizTypDropdown_Apparel,WaitStrategy.PRESENCE);
        }else if (bizType.equalsIgnoreCase("Electronics/Mobile shops")){
            click(bizTypDropdown_Electronics,WaitStrategy.PRESENCE);
        }else if (bizType.equalsIgnoreCase("Medical Store/ Local Pharmacy")){
            click(bizTypDropdown_Medical,WaitStrategy.PRESENCE);
        }else if (bizType.equalsIgnoreCase("Others")){
            click(bizTypDropdown_Others,WaitStrategy.PRESENCE);

        }else
            System.out.printf("Please provide business type");
        return this;
    }


//For workDetails Input Field

    private final By  workDetailsLabel_empDet= By.xpath("//*[@id='sourceOfIncomeInput']/label");

    private final By  workDetailsInput_empDet= By.xpath("//*[@id='sourceOfIncomeInput']/div[1]/div[2]");

    private final By  workDetailsDropdown_driver= By.xpath("//*[@id='sourceOfIncomeInput']/div[1]/div[3]/ul/li[@class='multiselect__element']/span[span='Partners of Ola/Uber/Meru/Local commute operators']/span");

    private final By  workDetailsDropdown_delivery= By.xpath("//*[@id='sourceOfIncomeInput']/div[1]/div[3]/ul/li[@class='multiselect__element']/span[span='Partners of Delivery apps (Swiggy/Zomato/Dunzo etc..)']/span");

    private final By workDetailsDropdown_skilledProfessional = By.xpath("//*[@id='sourceOfIncomeInput']/div[1]/div[3]/ul/li[@class='multiselect__element']/span[span='Skilled professionals (Tutions/Salons/Urban clap partners)']/span");

    private final By workDetailsDropdown_others = By.xpath("//*[@id='sourceOfIncomeInput']/div[1]/div[3]/ul/li[@class='multiselect__element']/span[span='Others']/span");

    private final By workDetailsError_empDet = By.xpath("//*[@id='sourceOfIncomeInput']/div[2]");

    public  SlushEmploymentDetailsScreen selectWorkDetails(String workDetails){
        click(workDetailsInput_empDet,WaitStrategy.CLICKABLE);
        if (workDetails.equalsIgnoreCase("Partners of Ola/Uber/Meru/Local commute operators")){
            click(workDetailsDropdown_driver,WaitStrategy.PRESENCE);
        }else if (workDetails.equalsIgnoreCase("Partners of Delivery apps (Swiggy/Zomato/Dunzo etc..)")){
            click(workDetailsDropdown_delivery,WaitStrategy.PRESENCE);
        }else if (workDetails.equalsIgnoreCase("Skilled professionals (Tutions/Salons/Urban clap partners)")){
            click(workDetailsDropdown_skilledProfessional,WaitStrategy.PRESENCE);
        }else if (workDetails.equalsIgnoreCase("Others")){
            click(workDetailsDropdown_others,WaitStrategy.PRESENCE);
        }else
            System.out.printf("Please provide work details");
        return this;
    }


//For monthlyIncome input field

    private final By monthIncomeLabel_empDet = By.xpath("//*[@id='sourceOfIncome_Others']/label");

    private final By  monthIncomeInput_empDet= By.xpath("//*[@id='netIncome']");

    private final By monthIncomeError_empDet = By.xpath("//*[@id='sourceOfIncome_Others']/div");

    public  SlushEmploymentDetailsScreen enterNetMonthlyIncome(String netMonthlyIncome){
        sendKeys(monthIncomeInput_empDet,netMonthlyIncome,WaitStrategy.PRESENCE);
        return this;
    }

//For annual turnover input

    private final By annualTurnoverLabel_empDet = By.xpath("//*[@id='annualTurnover_Others']/label");

    private final By  annualTurnover_empDet= By.xpath("//*[@id='annualTurnover']");

    private final By  annualTurnoverError_empDet= By.xpath("//*[@id='sourceOfIncomeInput']/div[2]");

    public  SlushEmploymentDetailsScreen enterAnnualTurnover(String annualTurnover){
        sendKeys(annualTurnover_empDet,annualTurnover,WaitStrategy.PRESENCE);
        return this;
    }

//For Other Source of Income

    private final By incomeOtherSrcLabel_empDet = By.xpath("//*[@id='sourceOfIncome_OthersInput']/label");

    private final By incomeOtherSrc_empDet = By.xpath("//*[@id='sourceOfIncome_OthersInput']/input");

    private final By incomeOtherError_empDet = By.xpath("//*[@id='sourceOfIncome_OthersInput']/div");


    public  SlushEmploymentDetailsScreen enterOtherSourceOfIncome(String otherSourceOfIncome){
        sendKeys(incomeOtherSrc_empDet,otherSourceOfIncome,WaitStrategy.PRESENCE);
        return this;
    }

//submit employment details
    private final By  submitEmploymentDetails_empDet= By.xpath("//button[@type='submit']");

    public  SlushEmploymentDetailsScreen submitEmploymentDetails(){
        click(submitEmploymentDetails_empDet,WaitStrategy.PRESENCE);
        return this;
    }

}
