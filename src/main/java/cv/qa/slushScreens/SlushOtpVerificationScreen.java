package cv.qa.slushScreens;

import cv.qa.base.UiBase;
import cv.qa.enums.WaitStrategy;
import org.openqa.selenium.By;

public class SlushOtpVerificationScreen extends UiBase {

    private String screenName_Otp="//div[@class='cvLoanHeaderTitle']";

    private String input_Otp="//input[@id='otp']";

    private String submit_Otp="//button[@id='otpSubmit']";

    private String resend_Otp="//button[@id='resendOtp']";

    private String resendCountdown_Otp="//span[@class='countDown']";


//    public  SlushOtpVerificationScreen getSlushOtpVerificationScreenName(){
//
//        getPageTitle(By.xpath(), WaitStrategy.VISIBLE);
//        return this;
//    }

    public  SlushOtpVerificationScreen enterOtp(String userVerificationOtp){
        sendKeys(By.xpath(input_Otp), userVerificationOtp, WaitStrategy.VISIBLE);
        return this;
    }

    public  SlushOtpVerificationScreen clickOnsubmitOtp(){
        click(By.xpath(submit_Otp),WaitStrategy.PRESENCE);
        return this;
    }

    public  SlushOtpVerificationScreen resendOtp(){
        return this;
    }

    public String getResendOtpCountdown(){
        String countdown = getText(By.xpath(resendCountdown_Otp), WaitStrategy.VISIBLE);
        return countdown;
    }

}
