package cv.qa.slushScreens;

import cv.qa.base.UiBase;
import cv.qa.enums.WaitStrategy;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class SlushRepaymentAccountDetailsScreen extends UiBase {

    private final By repaymentAccount_primaryAccount=By.xpath("//label[@class='checkboxradio_btn font-medium' and @for='0']");
    private final By repaymentAccount_nonPrimaryAccount=By.xpath("//label[@class='checkboxradio_btn font-medium' and @for='1']");

    private final By repaymentAccount_bankDropdown=By.xpath("//*[@id='cvLoan_repayForm_section']/article[1]/div/div/div/div[1]/div/div[1]");
    private final By repaymentAccount_bankInput=By.xpath("//*[@id='cvLoan_repayForm_section']/article[1]/div/div/div/div[1]/div/div[2]/input");
    private final By repaymentAccount_bankdiv=By.xpath("//*[@id='cvLoan_repayForm_section']/article[1]/div/div/div/div[1]/div/div[3]");

    public SlushRepaymentAccountDetailsScreen nachSetupNonPrimaryBank() {
        click(repaymentAccount_nonPrimaryAccount, WaitStrategy.CLICKABLE);
        click(repaymentAccount_submit,WaitStrategy.CLICKABLE);
        return this;
    }
    public SlushRepaymentAccountDetailsScreen selectBank(String bakName){
        click(repaymentAccount_bankDropdown, WaitStrategy.CLICKABLE);
        sendKeys(repaymentAccount_bankInput,bakName,WaitStrategy.CLICKABLE);
        click(repaymentAccount_bankdiv,WaitStrategy.PRESENCE);
        return this;
    }

    private final By repaymentAccount_accountNumber=By.xpath("//*[@id='accountNumber']");
    private final By repaymentAccount_confirmAccountNumber=By.xpath("//*[@id='reaccountNumber']");

    public SlushRepaymentAccountDetailsScreen enterAccountNumber(String accountNumber){
        sendKeys(repaymentAccount_accountNumber,accountNumber,WaitStrategy.CLICKABLE);
        sendKeys(repaymentAccount_confirmAccountNumber,accountNumber,WaitStrategy.CLICKABLE);
        return this;
    }

    private final By repaymentAccount_ifscInput=By.xpath("//*[@id='ifscCode']");

    public SlushRepaymentAccountDetailsScreen enterIfsc(String ifsc){
        sendKeys(repaymentAccount_ifscInput,ifsc,WaitStrategy.CLICKABLE);
        return this;
    }

    private final By repaymentAccount_modeInternetBanking=By.xpath("//label[@for='netBanking']");
    private final By repaymentAccount_modeDebitCard=By.xpath("//label[@for='debitCard']");

    public SlushRepaymentAccountDetailsScreen selectRepaymentMode(String repaymentMode){
        if (repaymentMode.equalsIgnoreCase("net banking")){
            click(repaymentAccount_modeInternetBanking,WaitStrategy.CLICKABLE);
        }else if (repaymentMode.equalsIgnoreCase("debit card")){
            click(repaymentAccount_modeDebitCard,WaitStrategy.CLICKABLE);
        }else
            System.out.printf("Provide repayment setup mode");
        return this;
    }

    private final By repaymentAccount_submit=By.xpath("//*[@id='accountDetailsSubmit']");

    public SlushRepaymentAccountDetailsScreen submitRepaymentSetup(){
        click(repaymentAccount_submit,WaitStrategy.CLICKABLE);
        return this;
    }



}
