package cv.qa.slushScreens;

import cv.qa.base.UiBase;
import cv.qa.enums.WaitStrategy;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SlushReviewPendingScreen extends UiBase {

    private final By screenName_reviewPending=By.xpath("//div[@class='cvLoanHeader']");

    private final By okay_reviewPending=By.xpath("//button[@type='submit']");

    private final By message_reviewPending=By.xpath("//*[@id='messageScreens']/div[2]/div/div/span");


    public SlushReviewPendingScreen click_ok_onReviewPending(){
        click(okay_reviewPending,WaitStrategy.CLICKABLE);
        return this;
    }
}
