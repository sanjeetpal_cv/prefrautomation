package cv.qa.slushScreens;

import cv.qa.base.UiBase;
import cv.qa.enums.WaitStrategy;
import org.openqa.selenium.By;

public final class SlushLaunchScreen extends UiBase {

    private String activateNow_launchScreen="//button[@type='submit']";

    public SlushLaunchScreen clickActivateNow(){
        click(By.xpath(activateNow_launchScreen), WaitStrategy.CLICKABLE);
        return this;
    }
}
