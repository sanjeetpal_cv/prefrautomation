package cv.qa.slushScreens;

import cv.qa.base.UiBase;
import cv.qa.enums.WaitStrategy;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SlushTentativeOfferScreen extends UiBase {

    private final By tentativeOffer_proceedToUploadStatement= By.xpath("//button[@type='submit' and contains(text(),'Upload Bank Statement')]");
    private final By tentativeOffer_bankDropdown=By.xpath("//*[@id='noCustomHeader']/article[1]/div/div/div/div[1]/div/div[1]");
    private final By tentativeOffer_bankInput=By.xpath("//*[@id='noCustomHeader']/article[1]/div/div/div/div[1]/div/div[2]/input");
    private final By tentativeOffer_bankdiv=By.xpath("//*[@id='noCustomHeader']/article[1]/div/div/div/div[1]/div/div[3]");

    private final By tentativeOffer_netBanking=By.xpath("//*[@class='finbitEachOption']/div[contains(text(),'Net Banking')]");
    private final By tentativeOffer_uploadStatement=By.xpath("//*[@class='finbitEachOption']/div[contains(text(),'Upload bank statement')]");
    private final By tentativeOffer_uploadEachDocButton=By.xpath("//*[@id='uploadDocumentsSubmitButton' and contains(text(),'+ Upload Statements')]");
    public SlushTentativeOfferScreen uploadBankStatement(String bakName, String fileNameWithExtension,String statementPassword){
        click(tentativeOffer_proceedToUploadStatement, WaitStrategy.CLICKABLE);
        click(tentativeOffer_bankDropdown, WaitStrategy.CLICKABLE);
        sendKeys(tentativeOffer_bankInput,bakName,WaitStrategy.CLICKABLE);
        click(tentativeOffer_bankdiv,WaitStrategy.PRESENCE);
        click(tentativeOffer_uploadStatement,WaitStrategy.CLICKABLE);
        uploadFileRobotClass(tentativeOffer_uploadEachDocButton,fileNameWithExtension,WaitStrategy.CLICKABLE);
        sendKeys(By.xpath("//*[@id='passwordInput"+fileNameWithExtension+"']"),statementPassword,WaitStrategy.CLICKABLE);
        return this;
    }

    private final By tentativeOffer_submitSelectedOffer= By.xpath("//*[@id='accountDetailsSubmit' and contains(text(),'Save all and proceed')]");

    public SlushTentativeOfferScreen saveStatementAndProceed() {
        click(tentativeOffer_submitSelectedOffer, WaitStrategy.CLICKABLE);
        return this;
    }


    private final By tentativeOffer_feedbackLink= By.xpath("//*[@class='offerFeebackText' and contains(text(),'Didn’t like the offer')]");

    private final By tentativeOffer_feedbackModlOverlay= By.xpath("//*[@id='loanModal']/div[2]");

    private final By tentativeOffer_feedbackModalClose= By.xpath("//*[@id='loanModal']//*[@class='loanModalClose']");

    private final By tentativeOffer_feedbackModalList= By.xpath("//*[@id='radioBlock']/div");

    private final By tentativeOffer_feedbackModalSubmit= By.xpath("//*[@class='loanModalFooter']//*[@type='submit']");




}
