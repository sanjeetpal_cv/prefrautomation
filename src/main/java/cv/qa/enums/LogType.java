package cv.qa.enums;

public enum LogType {

    PASS,
    FAIL,
    SKIP,
    INFO,
    CONSOLE,
    EXTENTANDCONSOLE
}
