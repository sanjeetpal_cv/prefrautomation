package cv.qa.enums;

public enum WaitStrategy {

    CLICKABLE,
    PRESENCE,
    VISIBLE,
    NONE

}
