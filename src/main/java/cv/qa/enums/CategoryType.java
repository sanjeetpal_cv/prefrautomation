package cv.qa.enums;

public enum CategoryType {

    REGRESSION,
    SMOKE,
    SANITY,
    MINIREGRESSION;

}
