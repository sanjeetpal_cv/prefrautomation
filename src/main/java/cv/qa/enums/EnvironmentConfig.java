package cv.qa.enums;

public enum EnvironmentConfig {

    testMarketplace,
    betaMarketplace_base_url,
    uatMarketplace_base_url,
    marketplaceUri,
    processUri,
    prefrUri,
    hfcUri,
    x_tenant_name_test,
    x_tenant_name_beta,
    x_tenant_name_uat,
    URL;

}
