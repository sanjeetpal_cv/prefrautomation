package cv.qa.drivers;

import cv.qa.constants.FrameworkConstants;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.Objects;

public final class Driver {

    private Driver(){

    }

    public static void initWebDriver(String Browser) {
        if (Browser.equalsIgnoreCase("chrome")){
            if (Objects.isNull(WebDriverManager.getDriver())) {
                System.setProperty("webdriver.chrome.driver", FrameworkConstants.getChromeDriverPath());
                WebDriverManager.setDriver(new ChromeDriver());
            }
        }
        else if (Browser.equalsIgnoreCase("firefox")){
            if (Objects.isNull(WebDriverManager.getDriver())) {
                System.setProperty("webdriver.gecko.driver", FrameworkConstants.getFirefoxDriverPath());
                WebDriverManager.setDriver(new FirefoxDriver());
            }
        }
    }



    public static void quitWebDriver(){

        if (Objects.nonNull(WebDriverManager.getDriver())) {
            WebDriverManager.getDriver().quit();
        }
    }
}
