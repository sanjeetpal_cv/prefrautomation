//package cv.qa.factories;
//
//import cv.qa.drivers.WebDriver;
//
//import java.net.MalformedURLException;
//import java.net.URL;
//
//public final class WebDriverFactory {
//
//    private WebDriverFactory(){
//
//    }
//
//
//    public static WebDriver getDriver(String browser,String version) throws MalformedURLException {
//
//        WebDriver driver=null;
//
//        String runmode = PropertyUtils.get(ConfigProperties.RUNMODE);
//        if(browser.equalsIgnoreCase("chrome")) {
//            if(runmode.equalsIgnoreCase("remote")) {
//                DesiredCapabilities cap = new DesiredCapabilities();
//                cap.setBrowserName(BrowserType.CHROME);
//                cap.setVersion(version);
//                driver =new RemoteWebDriver(new URL(PropertyUtils.get(ConfigProperties.SELENIUMGRIDURL)), cap);
//            }
//            else {
//                WebDriverManager.chromedriver().setup();
//                driver = new ChromeDriver();
//            }
//        }
//        else if(browser.equalsIgnoreCase("firefox")) {
//
//            if(runmode.equalsIgnoreCase("remote")) {
//                DesiredCapabilities cap = new DesiredCapabilities();
//                cap.setBrowserName(BrowserType.FIREFOX);
//                cap.setVersion(version);
//                driver =new RemoteWebDriver(new URL(PropertyUtils.get(ConfigProperties.SELENIUMGRIDURL)), cap);
//            } else {
//                WebDriverManager.firefoxdriver().setup();
//                driver = new FirefoxDriver();
//            }
//        }
//        return driver;
//    }
//}
