package cv.qa.constants;

import cv.qa.enums.ConfigProperties;
import cv.qa.utils.PropertiesUtil;

import java.time.Duration;

public final class FrameworkConstants {

    private FrameworkConstants() {

    }

    public static final String ENVIRONMENT = "test";
    private static final Duration EXPLICITWAIT = Duration.ofSeconds(10);
    private static final String RESOURCES_PATH = System.getProperty("user.dir") + "\\src\\test\\resources";


    private static final String CHROME_DRIVER_PATH = RESOURCES_PATH + "\\executables\\chromedriver.exe";
    private static final String FIREFOX_DRIVER_PATH = RESOURCES_PATH+"\\executables\\geckodriver.exe";

    private static final String CONFIG_FILE_PATH = RESOURCES_PATH + "\\configs\\config.properties";
    private static final String ENVIRONMENT_CONFIG_PATH = RESOURCES_PATH + "\\configs\\environmentConfig.properties";
    private static final String TENANT_CONFIG_PATH_JSON = RESOURCES_PATH+ "\\configs\\tenantConfig.json";


    public static final String EXTENT_CONFIG_XML = RESOURCES_PATH+"\\configs\\extentConfig.xml";

    private static final String EXTENTREPORTFOLDERPATH = System.getProperty("user.dir")+"/extent-test-output/";
    private static String extentReportFilePath = "";


    private static final String SLUSH_APPLICATION_DATA_FILE_PATH = RESOURCES_PATH+"\\testData\\PrefrTestData.xlsx";

    private static final String SLUSH_JOURNEY_FILE_DIRECTORY = RESOURCES_PATH+"\\testData\\slushJourneyFiles\\";

    private static final String SLUSH_DATA_SHEETNAME ="slushMasterData";

    public static Duration getExplicitwait() {
        return EXPLICITWAIT;
    }
    public static String getChromeDriverPath() {
        return CHROME_DRIVER_PATH;
    }
    public static String getEnvironmentConfigPath() {
        return ENVIRONMENT_CONFIG_PATH;
    }

    public static String getConfigFilePath() {
        return CONFIG_FILE_PATH;
    }

    public static String getSlushJourneyFiles() {
        return SLUSH_JOURNEY_FILE_DIRECTORY;
    }


    public static String getTenantConfigPathJson(){
        return TENANT_CONFIG_PATH_JSON;
    }

    public static String getEnvironment(){
        return ENVIRONMENT;
    }


    public static String getFirefoxDriverPath() {
        return FIREFOX_DRIVER_PATH;
    }

    public static String getSlushApplicationDataFilePath(){
        return SLUSH_APPLICATION_DATA_FILE_PATH;
    }

    public static String getExtentConfigXml(){
        return EXTENT_CONFIG_XML;
    }

    private static String createReportPath()  {
        if(PropertiesUtil.get(ConfigProperties.OVERRIDEREPORTS).equalsIgnoreCase("no")) {
            return EXTENTREPORTFOLDERPATH+System.currentTimeMillis()+"/index.html";
        }
        else {
            return EXTENTREPORTFOLDERPATH+"/index.html";
        }
    }

    public static String getExtentReportFilePath()  {
        if(extentReportFilePath.isEmpty()) {
            extentReportFilePath = createReportPath();
        }
        return extentReportFilePath;
    }

    public static String getSlushDataSheetname(){
        return SLUSH_DATA_SHEETNAME;
    }

}
