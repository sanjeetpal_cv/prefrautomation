package cv.qa.thridPartyScreens;

import cv.qa.base.UiBase;
import cv.qa.drivers.Driver;
import cv.qa.drivers.WebDriverManager;
import cv.qa.enums.WaitStrategy;
import cv.qa.slushScreens.SlushRepaymentAccountDetailsScreen;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Wait;

public class DigioScreen extends UiBase {

    private final By digio_ScreenName= By.xpath("//*[@id='loanJourneyContainer']/div[1]/div/section/article/h5[contains(text(),'Digio API Setup')]");
    private final By digio_ScreenIframe= By.id("digio-ifm-1658758159910");

    private final By digio_mandateApiConsent= By.xpath("//input[@type='checkbox' and @ng-model='consent.apiMandateConsent']");
    private final By digio_mandateWithDebitCard= By.xpath("//*[@id='authModeDebit']");
    private final By digio_mandateWithNetBanking= By.xpath("//*[@id='authModeNet']");
    private final By digio_mandateSubmitBtn= By.xpath("//*[@type='submit' and @value='Submit']");
    private final By digio_mandateCancelBtn= By.xpath("//cancel-process/div[contains(text(),'Cancel')]");

    private final By digio_RetryRepaymentSetupBtn= By.xpath("//button[@type='submit' and contains(text(),'Retry')]");



    public DigioScreen submitDigioConsent(){
        click(digio_RetryRepaymentSetupBtn, WaitStrategy.CLICKABLE);
        return this;
    }

}
